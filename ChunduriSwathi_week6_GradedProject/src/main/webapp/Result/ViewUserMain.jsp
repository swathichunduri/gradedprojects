<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Magic of Books</title>
</head>
<body>
	<div align="center">
		<h1 style="color: Green">WELCOME TO MAGIC OF BOOKS</h1>
		<h2 style="color: Orange">DASHBOARD</h2>
		<h3>
			<a href="RetrieveUserBooks">ShowBooks</a>
		</h3>
		<h3>
			<a href="SearchBookById1">SearchBookById</a>
		</h3>
		<h3>
			<a href="SearchBookByName">SearchBookByName</a>
		</h3>
		<h3>
			<a href="SearchBookByAuthor1">SearchBookByAuthor</a>
		</h3>
		<h3>
			<a href="SearchBookByPublication">SearchBookByPublication</a>
		</h3>
		<h3>
			<a href="SortBooksByPrice">SortBooksByPrice</a>
		</h3>
		<h3>
			<a href="SearchBooksByPriceRange">SearchBookByPriceRange</a>
		</h3>
		<a href="logout"
		onclick="return confirm('Are you sure want to logout???')">LOGOUT</a>
	</div>
</body>
</html>