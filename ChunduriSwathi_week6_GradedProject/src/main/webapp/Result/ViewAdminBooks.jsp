<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Magic of Books</title>
</head>
<body>
	<div align="center">
		<h1>Books</h1>
		<h3>
			<a href="AdminView">DASHBOARD</a>
		</h3>
		<table border="1">
			<th style="color: Red">SNO</th>
			<th style="color: Red">BOOK ID</th>
			<th style="color: Red">BOOK NAME</th>
			<th style="color: Red">AUTHOR NAME</th>
			<th style="color: Red">PUBLICATION</th>
			<th style="color: Red">PRICE</th>
			<th style="color: Red">ACTIONS</th>

			<c:forEach var="book" items="${bookslist}" varStatus="status">
				<tr>
					<td>${status.index + 1}</td>
					<td>${book.bookId}</td>
					<td>${book.bookName}</td>
					<td>${book.authorName}</td>
					<td>${book.publication}</td>
					<td>${book.price}</td>
					<td><a href="editBook?bookId=${book.bookId}">Edit</a>
						&nbsp;&nbsp;&nbsp;&nbsp; <a
						href="deleteBook?bookId=${book.bookId}">Delete</a></td>

				</tr>
			</c:forEach>
		</table>
		<a href="logout" 
		onclick="return confirm('Are you sure want to logout???')">LOGOUT</a>
	</div>
</body>
</html>