<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Magic of Books</title>
</head>
<body>
	<div align="center">
		<h1>Books</h1>
		<h3>
			<a href="UserView">DASHBOARD</a>
		</h3>
		<table border="1">
			<th style="color: Red">SNO</th>
			<th style="color: Red">BOOK ID</th>
			<th style="color: Red">BOOK NAME</th>
			<th style="color: Red">AUTHOR NAME</th>
			<th style="color: Red">PUBLICATION</th>
			<th style="color: Red">PRICE</th>
			<th style="color: Red">ACTIONS</th>
			<c:forEach var="book" items="${booklist1}" varStatus="status">
				<tr>
					<td>${status.index + 1}</td>
					<td>${book.bookId}</td>
					<td>${book.bookName}</td>
					<td>${book.authorName}</td>
					<td>${book.publication}</td>
					<td>${book.price}</td>
					<td><a href="AddToFavourite?bookId=${book.bookId}"
						onclick="return confirm('Book added to Favourite sucessfully')">Like</a>
						&nbsp;&nbsp;&nbsp;&nbsp; 
						<a href="ReadLater?bookId=${book.bookId}"
						onclick="return confirm('Book added to Read Later sucessfully')">ReadLater</a>
					</td>

				</tr>
			</c:forEach>
		</table>
		<a href="logout"
		onclick="return confirm('Are you sure want to logout???')">LOGOUT</a>
	</div>
</body>
</html>