<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Item</title>
</head>
<body>
	<div align="center">
		<h1 style="color: green">BOOK UPDATE FORM</h1>
		<form:form method="POST" action="getUpdatedBooks"
			modelAttribute="command">
			<table>
				<tr>
					<td><form:label path="bookName">Enter Book Name to Update : </form:label></td>
					<td><form:input path="bookName" /></td>
				</tr>
				<tr>
					<td><form:label path="authorName">Enter Author Name to Update : </form:label></td>
					<td><form:input path="authorName" /></td>
				</tr>
				<tr>
					<td><form:label path="publication">Enter Publication to Update : </form:label></td>
					<td><form:input path="publication" /></td>
				</tr>
				<tr>
					<td><form:label path="price">Enter Price to Update : </form:label></td>
					<td><form:input path="price" /></td>
				</tr>
				<tr>
					<td align="center" colspan="2"><input type="submit"
						value="Update" /></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>