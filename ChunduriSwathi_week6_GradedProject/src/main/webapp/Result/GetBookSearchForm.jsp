<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Magic Of Books</title>
</head>
<body>
	<div align="center">
		<h1>Books</h1>
		<h3>
			<a href="AdminView">DASHBOARD</a>
		</h3>
		<table border="1">
			<th style="color:Red">SNO</th>
			<th style="color:Red">BOOK ID</th>
			<th style="color:Red">BOOK NAME</th>
			<th style="color:Red">AUTHOR NAME</th>
			<th style="color:Red">PUBLICATION</th>
			<th style="color:Red">PRICE</th>
			<c:forEach var="book" items="${abook1}" varStatus="status">
				<tr>
					<td>${status.index + 1}</td>
					<td>${book.bookId}</td>
					<td>${book.bookName}</td>
					<td>${book.authorName}</td>
					<td>${book.publication}</td>
					<td>${book.price}</td>
				</tr>
			</c:forEach>
		</table>
		<br>
		<a href="logout"
		onclick="return confirm('Are you sure want to logout???')">LOGOUT</a>
	</div>
</body>
</html>
