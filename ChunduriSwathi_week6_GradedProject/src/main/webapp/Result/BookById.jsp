<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Magic of Books</title>
</head>
<body>
	<div align="center">
		<h1 style="color: Green">BOOK SEARCH USING BOOKID</h1>
		<form:form method="POST" action="GetSearchBookById1"
			modelAttribute="obj1">
			<table>
				<tr>
					<td><form:label path="bookId">Enter Book Id : </form:label></td>
					<td><form:input path="bookId" /><br></td>
				</tr>

				<tr>
					<td align="center" colspan="2"><input type="submit"
						value="SEARCH" /></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>