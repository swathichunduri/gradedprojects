package com.magicofbooks.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.magicofbooks.entity.Books;
@Repository
public interface BooksRepository extends JpaRepository<Books,Integer>
{
	//finds list of books with that id.
	public List<Books> findByBookId(int bookId);
	//finds list of books with that name.
	public List<Books> findByBookName(String bookName);
	//finds list of books with that author name.
	public List<Books> findByAuthorName(String author_Name);
	//finds list of books with that publication name.
	public List<Books> findByPublication(String publication);
	//sorts the books in ascending order according to price attribute.
	public List<Books> findByOrderByPriceAsc();
	//List the books having price greater than given value
	@Query("select book from Books book where price>?1")
	public List<Books> getBooksByPrice(int price);
}
