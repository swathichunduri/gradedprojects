package com.magicofbooks.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.magicofbooks.entity.Admin;
@Repository
public interface AdminsRepository extends JpaRepository<Admin,String>
{

}
