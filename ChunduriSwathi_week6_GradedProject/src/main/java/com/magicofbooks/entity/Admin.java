package com.magicofbooks.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="admins")//name of table in database
public class Admin 
{
	@Id//id represents primary key
	private String adminName;
	private String password;
	private String emailId;
	private String phoneNumber;
	//getters and setters of the attributes
	public String getAdminName() 
	{
		return adminName;
	}
	public void setAdminName(String adminName) 
	{
		this.adminName = adminName;
	}
	public String getPassword() 
	{
		return password;
	}
	public void setPassword(String password) 
	{
		this.password = password;
	}
	public String getEmailId() 
	{
		return emailId;
	}
	public void setEmailId(String emailId) 
	{
		this.emailId = emailId;
	}
	public String getPhoneNumber() 
	{
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) 
	{
		this.phoneNumber = phoneNumber;
	}
}
