package com.magicofbooks.controller;

import java.io.PrintWriter;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController 
{
	//directly comes to the welcome page 
	@RequestMapping("/")
	public String getWelcomePage()
	{
		return "Welcome";
	}
	//when click on logout successfully
	@RequestMapping("/logout")
	public String getSignOut()
	{
		return "Welcome";
	}
}
