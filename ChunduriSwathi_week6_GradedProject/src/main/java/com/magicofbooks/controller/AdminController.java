package com.magicofbooks.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.magicofbooks.entity.Admin;
import com.magicofbooks.repository.AdminsRepository;

@Controller
public class AdminController
{
	@Autowired
	AdminsRepository arep;
	
	@RequestMapping("/AdminRegister")
	public ModelAndView getForm()
	{
		return new ModelAndView("AdminRegisterForm","admin1",new Admin());
	}
	
	@RequestMapping("/AdminRegistration")
	public String registering(@ModelAttribute ("admin1") Admin admin)
	{
		//inserting data into table.
		arep.save(admin);
		return "redirect:/AdminView";
	}
	
	@RequestMapping("/AdminLink")
	public ModelAndView getLoginForm()
	{
		return new ModelAndView("AdminLoginForm","admin2",new Admin());
	}
	
	@RequestMapping("/AdminLogin")
	public String checkingUser(@ModelAttribute ("admin2") Admin admin) 
	{
		//accepting the adminName and password
		String aname=admin.getAdminName();
		String apwd=admin.getPassword();
		List<Admin> adminlist=arep.findAll();
		//checking the values with the database values
		for(Admin a1:adminlist)
		{
			if(a1.getAdminName().equals(aname) && a1.getPassword().equals(apwd))
				return "redirect:/AdminView";
		}
		return "redirect:/AdminLink";
	}
}
