package com.magicofbooks.controller;

import java.util.*;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.magicofbooks.entity.Books;
import com.magicofbooks.repository.BooksRepository;

@Controller
public class UserBooksController 
{
	@Autowired 
	BooksRepository brep;
	Books dbook1;
	@RequestMapping("/UserView")
	public String getOperationsOfUser()
	{
		return "ViewUserMain";
	}
	
	@RequestMapping("/RetrieveUserBooks")
	public ModelAndView getUserBooks(ModelAndView model)
	{
		//retrieves all the books into a list
		List<Books> booklist1=brep.findAll();
		model.addObject("booklist1",booklist1);
		model.setViewName("ViewUserBooks");
		return model;
	}
	
	@RequestMapping("/SearchBookById1")
	public ModelAndView getBookById()
	{
		return new ModelAndView("BookById","obj1",new Books());
	}
	
	@RequestMapping("/GetSearchBookById1")
	public ModelAndView getBookid(@ModelAttribute ("obj1") Books b)
	{
		//searches book by using id of the book.
		List<Books> book_id=brep.findByBookId(b.getBookId());
		return new ModelAndView("GetBookById","book1",book_id);
	}
	
	@RequestMapping("/SearchBookByName")
	public ModelAndView getBookByName()
	{
		return new ModelAndView("BookByName","obj2",new Books());
	}
	
	@RequestMapping("/GetSearchBookByName")
	public ModelAndView getBookName(@ModelAttribute ("obj2") Books b)
	{
		//searches book by using name of the book.
		List<Books> book_Name=brep.findByBookName(b.getBookName());
		return new ModelAndView("GetBookByName","book2",book_Name);
	}
	
	@RequestMapping("/SearchBookByAuthor1")
	public ModelAndView getBookByAuthorName()
	{
		return new ModelAndView("BookByAuthorName","obj3",new Books());
	}
	
	@RequestMapping("/GetSearchBookByAuthor1")
	public ModelAndView getAuthorName(@ModelAttribute ("obj3") Books b)
	{
		//searches book by using author name of the book.
		List<Books> author_Name=brep.findByAuthorName(b.getAuthorName());
		return new ModelAndView("GetBookByAuthorName","book3",author_Name);
	}
	
	@RequestMapping("/SearchBookByPublication")
	public ModelAndView getBookByPublication()
	{
		return new ModelAndView("BookByPublication","obj4",new Books());
	}
	
	@RequestMapping("/GetSearchBookByPublication")
	public ModelAndView getPublication(@ModelAttribute ("obj4") Books b)
	{
		//searches book by using publication name of the book.
		List<Books> publication=brep.findByPublication(b.getPublication());
		return new ModelAndView("GetBookByPublication","book4",publication);
	}
	
	@RequestMapping("/SortBooksByPrice")
	public ModelAndView sort()
	{
		//sorts book by price of the book
		List<Books> books=brep.findByOrderByPriceAsc();
		return new ModelAndView("SortedBooks","book5",books);
	}
	
	@RequestMapping("/SearchBooksByPriceRange")
	public ModelAndView getRange()
	{
		return new ModelAndView("PriceRangeForm","obj5",new Books());
	}
	
	@RequestMapping("/GetBooksBetweenPriceRange")
	public  ModelAndView getBooksByRange(@ModelAttribute ("obj5") Books b)
	{
		//searches for the books above the given price
		List<Books> min_price=brep.getBooksByPrice(b.getPrice());
		return new ModelAndView("RangeBooklist","book6",min_price);
	}
	
	@RequestMapping("/AddToFavourite")
	public String MakeBookFavourite(HttpServletRequest req)
	{
		//saves a book as liked
		int book_id = Integer.parseInt(req.getParameter("bookId"));
		Books book1=brep.getOne(book_id);
		book1.setAction("Like");
		brep.save(book1);
		return "redirect:/RetrieveUserBooks";
	}
	
	@RequestMapping("/ReadLater")
	public String MakeBookReadLater(HttpServletRequest req)
	{
		//saves a book as read later.
		int book_id = Integer.parseInt(req.getParameter("bookId"));
		Books book1=brep.getOne(book_id);
		book1.setAction("ReadLater");
		brep.save(book1);
		return "redirect:/RetrieveUserBooks";
	}
	
} 

