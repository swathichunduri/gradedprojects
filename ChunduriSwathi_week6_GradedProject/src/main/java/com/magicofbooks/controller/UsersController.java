package com.magicofbooks.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.magicofbooks.entity.Users;
import com.magicofbooks.repository.UsersRepository;

@Controller
public class UsersController 
{
	@Autowired
	UsersRepository urep;
	
	@RequestMapping("/UserRegister")
	public ModelAndView getForm()
	{
		return new ModelAndView("UserRegisterForm","user1",new Users());
	}
	
	@RequestMapping("/UserRegistration")
	public String registering(@ModelAttribute ("user") Users user)
	{
		//inserts value in the database.
		urep.save(user);
		return "redirect:/UserView";
	}
	
	@RequestMapping("/UserLink")
	public ModelAndView getLoginForm()
	{
		return new ModelAndView("UserLoginForm","user2",new Users());
	}
	
	@RequestMapping("/UserLogin")
	public String checkingUser(@ModelAttribute ("user2") Users user) 
	{
		String uname=user.getUserName();
		String upwd=user.getPassword();
		List<Users> userlist=urep.findAll();
		//checking the values with database 
		for(Users u1:userlist)
		{
			if(u1.getUserName().equals(uname) && u1.getPassword().equals(upwd))
				return "redirect:/UserView";
		}
		return "redirect:/UserLink";
	}
}
