package com.magicofbooks.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.magicofbooks.entity.Books;
import com.magicofbooks.repository.BooksRepository;

@Controller
public class AdminBooksController 
{
	Books dbook;
	@Autowired
	BooksRepository brep;

	@RequestMapping("/AdminView")
	public String getOperations() 
	{
		return "ViewAdminMain";
	}

	@RequestMapping("/RetrieveBooks")
	public ModelAndView getBooks(ModelAndView model) 
	{
		//Lists out all the books in the table using findAll() method
		List<Books> bookslist = brep.findAll();
		model.addObject("bookslist", bookslist);
		model.setViewName("ViewAdminBooks");
		return model;
	}

	@RequestMapping("/newBook")
	public ModelAndView getInsertForm()
	{
		//its calling the BookInsertForm
		return new ModelAndView("BookInsertForm", "book1", new Books());
	}

	@RequestMapping("/addBook")
	public String addingBook(@ModelAttribute("book1") Books book) 
	{
		//save function directly inserts the data into the database
		brep.save(book);
		return "redirect:/RetrieveBooks";
	}

	@RequestMapping("/deleteBook")
	public String makeBookDelete(HttpServletRequest request) 
	{
		int book_id = Integer.parseInt(request.getParameter("bookId"));
		//deleteById will directly delete the book
		brep.deleteById(book_id);
		return "redirect:/RetrieveBooks";
	}

	@RequestMapping("/editBook")
	public ModelAndView getUpdateForm(HttpServletRequest request) 
	{
		int book_id = Integer.parseInt(request.getParameter("bookId"));
		//getOne() function is used to retrieve the details of the book with that id.
		Books book = brep.getOne(book_id);
		ModelAndView model = new ModelAndView("BookUpdateForm", "command", book);
		dbook = book;
		return model;
	}

	@RequestMapping("/getUpdatedBooks")
	public String getBooks(@ModelAttribute("command") Books b1)
	{
		//here we are updating the values using getters and setters.
		dbook.setBookName(b1.getBookName());
		dbook.setAuthorName(b1.getAuthorName());
		dbook.setPublication(b1.getPublication());
		dbook.setPrice(b1.getPrice());
		brep.save(dbook);//saving the details of updated book.
		return "redirect:/RetrieveBooks";
	}

	@RequestMapping("/SearchBookById")
	public ModelAndView searchBook() 
	{
		return new ModelAndView("BookSearchForm", "bobj1", new Books());
	}

	@RequestMapping("/getSearchedBook")
	public ModelAndView getBook(@ModelAttribute ("bobj1") Books b) 
	{
		//finding book by giving specific id.
		List<Books> book_id=brep.findByBookId(b.getBookId());
		return new ModelAndView("GetBookSearchForm","abook1",book_id);
	}

	@RequestMapping("/SearchBookByAuthor")
	public ModelAndView searchBookByAuthor()
	{
		return new ModelAndView("AuthorBookSearch", "bobj2", new Books());
	}

	@RequestMapping("/getSearchedBookByAuthor")
	public ModelAndView getBookByAuthor(@ModelAttribute ("bobj2") Books b)
	{
		//finding the books by giving author name
		List<Books> author_name=brep.findByAuthorName(b.getAuthorName());
		return new ModelAndView("GetAuthorBookSearch","abook2",author_name);
	}

}
