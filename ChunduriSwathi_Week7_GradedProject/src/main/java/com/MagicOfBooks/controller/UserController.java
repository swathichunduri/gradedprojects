package com.MagicOfBooks.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.MagicOfBooks.model.User;
import com.MagicOfBooks.repository.UserRepository;

@Controller
public class UserController 
{
	@Autowired
	UserRepository urep;
	
	@RequestMapping("/UserLogin")
	public ModelAndView getLoginForm()
	{
		//user will get a form to enter his login details.
		return new ModelAndView("UserLoginForm","user1",new User());
	}
	
	@RequestMapping("/CheckUser")
	public String getLogin(@ModelAttribute ("user1")User user)
	{
		String uname=user.getUserName();
		String password=user.getPassword();
		//List all the users in the database.
		List<User> userlist=urep.findAll();
		for(User u1:userlist)
		{
			//checking the values with the database values..
			if(u1.getUserName().equals(uname) && u1.getPassword().equals(password))
				return "redirect:/UserDashboard";
		}
		return "redirect:/UserLogin";
	}

	@RequestMapping("/UserRegister")
	public ModelAndView getRegisterForm()
	{
		//here user gets the registration form to register.
		return new ModelAndView("UserRegisterForm","user2",new User());
	}
	
	@RequestMapping("/UserRegistration")
	public String getRegistered(@ModelAttribute ("user2")User user)
	{
		//the details entered will be saved in the database.
		urep.save(user);
		return "redirect:/UserDashboard";
	}
	
	@RequestMapping("/UserDashboard")
	public String getUserMain()
	{
		//displays the user dashBoard.
		return "UserDashBoard";
	}
}
