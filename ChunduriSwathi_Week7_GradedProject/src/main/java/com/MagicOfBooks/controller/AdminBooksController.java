package com.MagicOfBooks.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.MagicOfBooks.model.Book;
import com.MagicOfBooks.repository.BookRepository;

@Controller
public class AdminBooksController 
{
	Book dbook;
	@Autowired
	BookRepository brep;
	
	@RequestMapping("/NewBook")
	public ModelAndView getForm()
	{
		//it will return a form to insert the books into database..
		return new ModelAndView("BookInsertForm","book1",new Book());
	}
	
	@RequestMapping("/InsertBook")
	public String addBook(@ModelAttribute ("book1") Book book)
	{
		//directly saves the book details in the database.
		brep.save(book);
		return "redirect:/RetrieveBooks";
	}
	
	@RequestMapping("/RetrieveBooks")
	public ModelAndView getAllBooks(ModelAndView model)
	{
		//findAll() method will retrieve all the book details from the database.
		List<Book> bookslist=brep.findAll();
		model.addObject("bookslist", bookslist);
		model.setViewName("ViewAdminBooks");
		return model;
	}
	
	@RequestMapping("/DeleteBook")
	public String deletebook(HttpServletRequest request)
	{
		int book_id=Integer.parseInt(request.getParameter("bookId"));
		//deleting the book by using the bookid. 
		brep.deleteById(book_id);
		return "redirect:/RetrieveBooks";
	}
	
	@RequestMapping("/EditBook")
	public ModelAndView getEditForm(HttpServletRequest request)
	{
		int book_id=Integer.parseInt(request.getParameter("bookId"));
		//It will get the details of book based on the bookId.
		Book book=brep.getOne(book_id);
		dbook=book;
		return new ModelAndView("BookEditForm","book2",book);
	}
	
	@RequestMapping("/UpdateBook")
	public String getUpdatedBook(@ModelAttribute ("book2") Book book)
	{
		//using getters and setters for updating the values.
		dbook.setBookName(book.getBookName());
		dbook.setAuthorName(book.getAuthorName());
		dbook.setPublication(book.getPublication());
		dbook.setPrice(book.getPrice());
		brep.save(dbook);//saving the updated book details..
		return "redirect:/RetrieveBooks";
	}
	
	@RequestMapping("/SearchById")
	public ModelAndView searchBookByIDForm() 
	{
		return new ModelAndView("SearchById", "book3", new Book());
	}

	@RequestMapping("/GetSearchById")
	public ModelAndView getBookById(@ModelAttribute ("book3") Book book) 
	{
		//finding book by giving specific id.
		List<Book> book_id=brep.findByBookId(book.getBookId());
		return new ModelAndView("GetSearchById","list1",book_id);
	}
	
	@RequestMapping("/SearchByAuthor")
	public ModelAndView searchBookByAuthorForm()
	{
		return new ModelAndView("SearchByAuthor","book4",new Book());
	}
	
	@RequestMapping("/GetSearchByAuthor")
	public ModelAndView getBookByAuthor(@ModelAttribute ("book4") Book book)
	{
		//finding books by giving the author name.
		List<Book> author_name=brep.findByAuthorName(book.getAuthorName());
		return new ModelAndView("GetSearchByAuthor","list2",author_name);
	}
}

