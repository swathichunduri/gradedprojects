package com.MagicOfBooks.controller;

import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.MagicOfBooks.model.Book;
import com.MagicOfBooks.model.Favourite;
import com.MagicOfBooks.model.ReadLater;
import com.MagicOfBooks.repository.BookRepository;
import com.MagicOfBooks.repository.FavouriteRepository;
import com.MagicOfBooks.repository.ReadLaterRepository;



@Controller
public class UserBooksController 
{
	@Autowired 
	BookRepository brep;
	@Autowired 
	FavouriteRepository frep;
	@Autowired
	ReadLaterRepository readrep;
	Book dbook1;

	@RequestMapping("/RetrieveUserBooks")
	public ModelAndView getUserBooks(ModelAndView model)
	{
		//retrieves all the books into a list
		List<Book> booklist1=brep.findAll();
		model.addObject("booklist1",booklist1);
		model.setViewName("ViewUserBooks");
		return model;
	}
	
	@RequestMapping("/SearchBookById1")
	public ModelAndView getBookById()
	{
		return new ModelAndView("BookById","obj1",new Book());
	}
	
	@RequestMapping("/GetSearchBookById1")
	public ModelAndView getBookid(@ModelAttribute ("obj1") Book b)
	{
		//searches book by using id of the book.
		List<Book> book_id=brep.findByBookId(b.getBookId());
		return new ModelAndView("GetBookById","book1",book_id);
	}
	
	@RequestMapping("/SearchBookByName")
	public ModelAndView getBookByName()
	{
		return new ModelAndView("BookByName","obj2",new Book());
	}
	
	@RequestMapping("/GetSearchBookByName")
	public ModelAndView getBookName(@ModelAttribute ("obj2") Book b)
	{
		//searches book by using name of the book.
		List<Book> book_Name=brep.findByBookName(b.getBookName());
		return new ModelAndView("GetBookByName","book2",book_Name);
	}
	
	@RequestMapping("/SearchBookByAuthor1")
	public ModelAndView getBookByAuthorName()
	{
		return new ModelAndView("BookByAuthorName","obj3",new Book());
	}
	
	@RequestMapping("/GetSearchBookByAuthor1")
	public ModelAndView getAuthorName(@ModelAttribute ("obj3") Book b)
	{
		//searches book by using author name of the book.
		List<Book> author_Name=brep.findByAuthorName(b.getAuthorName());
		return new ModelAndView("GetBookByAuthorName","book3",author_Name);
	}
	
	@RequestMapping("/SearchBookByPublication")
	public ModelAndView getBookByPublication()
	{
		return new ModelAndView("BookByPublication","obj4",new Book());
	}
	
	@RequestMapping("/GetSearchBookByPublication")
	public ModelAndView getPublication(@ModelAttribute ("obj4") Book b)
	{
		//searches book by using publication name of the book.
		List<Book> publication=brep.findByPublication(b.getPublication());
		return new ModelAndView("GetBookByPublication","book4",publication);
	}
	
	@RequestMapping("/SortBooksByPrice")
	public ModelAndView sort()
	{
		//sorts book by price of the book
		List<Book> books=brep.findByOrderByPriceAsc();
		return new ModelAndView("SortedBooks","book5",books);
	}
	
	@RequestMapping("/SearchBooksByPriceRange")
	public ModelAndView getRange()
	{
		return new ModelAndView("PriceRangeForm","obj5",new Book());
	}
	
	@RequestMapping("/GetBooksBetweenPriceRange")
	public  ModelAndView getBooksByRange(@ModelAttribute ("obj5") Book b)
	{
		//searches for the books above the given price
		List<Book> min_price=brep.getBookByPrice(b.getPrice());
		return new ModelAndView("RangeBooklist","book6",min_price);
	}
	
	@RequestMapping("/AddToFavourite")
	public ModelAndView getForm(HttpServletRequest req)
	{
		int book_id=Integer.parseInt(req.getParameter("bookId"));
		Optional <Book> booklist=brep.findById(book_id);
		Book b=booklist.get();
		Favourite fav=new Favourite();
		fav.setBookId(b.getBookId());
		fav.setBookName(b.getBookName());
		fav.setUserName(fav.getUserName());
		return new ModelAndView("FavouriteForm","favourite",fav);
	}
	
	@RequestMapping("/saveFavourite")
	public String saveFavourite(@ModelAttribute ("favourite") Favourite fav)
	{
		//saves the book in the Favorite database.
		frep.save(fav);
		return "redirect:/UserDashboard";
	}
	
	@RequestMapping("/ReadLater")
	public ModelAndView getform(HttpServletRequest req)
	{
		int book_id=Integer.parseInt(req.getParameter("bookId"));	
		Optional <Book> booklist=brep.findById(book_id);
		Book b=booklist.get();
		ReadLater rl=new ReadLater();
		rl.setBookId(b.getBookId());
		rl.setBookName(b.getBookName());
		rl.setUserName(rl.getUserName());
		return new ModelAndView("ReadLaterForm","rlater",rl);
	}
	
	@RequestMapping("/saveToReadLater")
	public String saveToReadLater(@ModelAttribute("rlater") ReadLater readlater)
	{
		//saves the book in the read later database.
		readrep.save(readlater);
		return "redirect:/UserDashboard";
	}
} 

