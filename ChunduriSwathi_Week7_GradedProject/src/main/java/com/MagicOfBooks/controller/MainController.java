package com.MagicOfBooks.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
@RestController
public class MainController 
{
   @Autowired
   RestTemplate restTemplate;
   //its a class which can be used to call any external web service
   @RequestMapping(value = "/")
   public String getProductList() 
   {
      HttpHeaders headers = new HttpHeaders();
      //give the token generated here to continue with the application.
	  headers.setBearerAuth("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzd2F0aGkiLCJleHAiOjE2NjgzNDYwOTQsImlhdCI6MTY2ODMyODA5NH0.GfHOfQboLXKygwr7D7kzcTku1Qtp-99WvyH6y6pYAaOMbYwiwaKLzJqe2xHMPsKVivJBwre98JMiwACJ44wLjQ");
      HttpEntity <String> entity = new HttpEntity<String>(headers);
      //you can access the url by giving the http://localhost:8082/ 
      return restTemplate.exchange("http://localhost:8081/MagicOfBooks", HttpMethod.GET, entity, String.class).getBody();
   }
   //you can logout from the application 
   @RequestMapping("/logout")
   public ModelAndView signOut()
   {
	   return new ModelAndView("Welcome");
   }
}
