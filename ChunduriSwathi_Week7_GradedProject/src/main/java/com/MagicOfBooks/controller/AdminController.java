package com.MagicOfBooks.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.MagicOfBooks.model.Admin;
import com.MagicOfBooks.repository.AdminRepository;

@Controller
public class AdminController 
{
	@Autowired
	AdminRepository arep;
	
	@RequestMapping("/AdminLogin")
	public ModelAndView getLoginForm()
	{
		//here we will get the login form..
		return new ModelAndView("AdminLoginForm","admin1",new Admin());
	}
	
	@RequestMapping("/CheckAdmin")
	public String getLogin(@ModelAttribute ("admin1") Admin admin)
	{
		//accepting the adminName and password
		String aname=admin.getAdminName();
		String apwd=admin.getPassword();
		List<Admin> adminlist=arep.findAll();
		//checking the values with the database values
		for(Admin a1:adminlist)
		{
			if(a1.getAdminName().equals(aname) && a1.getPassword().equals(apwd))
				return "redirect:/AdminDashboard";
		}
		return "redirect:/AdminLogin";
	}
	
	@RequestMapping("/AdminRegister")
	public ModelAndView getRegisterForm()
	{
		//Here we will get the registration form
		return new ModelAndView("AdminRegisterForm","admin2",new Admin());
	}
	
	@RequestMapping("/AdminRegistration")
	public String getRegistration(@ModelAttribute ("admin2") Admin admin)
	{
		//the details sent in the form will be saved in the database using save() function..
		arep.save(admin);
		return "redirect:/AdminDashboard";
	}
	
	@RequestMapping("/AdminDashboard")
	public String getAdminPage()
	{
		//Displays adminDashBoard
		return "AdminDashBoard";
	}
}


