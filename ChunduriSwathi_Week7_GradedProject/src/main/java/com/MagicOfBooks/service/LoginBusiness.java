package com.MagicOfBooks.service;

import java.util.*;
public class LoginBusiness 
{
	public LoginService service;
	public LoginBusiness(LoginService service)
	{
		this.service=service;
	}
	public List<String> getUsers(String name)
	{
		List<String> retrieveUser=new ArrayList<String>();
		List<String> usersList=service.getUserDetails(name);
		for(String u1:usersList)
		{
			if(u1.contains("user1"))
			{
				retrieveUser.add(u1);
			}
		}
		return retrieveUser;
	}
}
