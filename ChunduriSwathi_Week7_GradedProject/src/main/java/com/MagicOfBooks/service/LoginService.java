package com.MagicOfBooks.service;

import java.util.List;

public interface LoginService 
{
	public List<String> getUserDetails(String name);
}
