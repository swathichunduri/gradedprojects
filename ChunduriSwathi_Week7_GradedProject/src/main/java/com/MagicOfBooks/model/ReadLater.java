package com.MagicOfBooks.model;

import javax.persistence.*;

@Entity
@Table(name="readlater")//name of table in the database.
public class ReadLater 
{
	//attributes of ReadLater class.
	@Id 
	@GeneratedValue(generator="readlater_seq",strategy=GenerationType.SEQUENCE)
	@SequenceGenerator(name="readlater_seq",sequenceName="readlater_seq",initialValue=1,allocationSize=1)
	//automatically generates the value.
	private int readId;
	private int bookId;
	private String bookName;
	private String userName;
	//getters and setters of this class.
	public int getReadId() 
	{
		return readId;
	}
	public void setReadId(int readId) 
	{
		this.readId = readId;
	}
	public int getBookId()
	{
		return bookId;
	}
	public void setBookId(int bookId)
	{
		this.bookId = bookId;
	}
	public String getBookName() 
	{
		return bookName;
	}
	public void setBookName(String bookName) 
	{
		this.bookName = bookName;
	}
	public String getUserName() 
	{
		return userName;
	}
	public void setUserName(String userName) 
	{
		this.userName = userName;
	}
}
