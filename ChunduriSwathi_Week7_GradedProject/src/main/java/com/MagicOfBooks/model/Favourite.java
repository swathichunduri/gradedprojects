package com.MagicOfBooks.model;

import javax.persistence.*;

@Entity
@Table(name="favourite")//name of table in the database.
public class Favourite 
{
	@Id 
	@GeneratedValue(generator="fav_seq",strategy=GenerationType.SEQUENCE)
	@SequenceGenerator(name="fav_seq",sequenceName="fav_seq",initialValue=1,allocationSize=1)
	//automatically generates the value.
	private int favId;
	private int bookId;
	private String BookName;
	private String userName;
	//getters and setters of this class.
	public int getFavId() 
	{
		return favId;
	}
	public void setFavId(int favId) 
	{
		this.favId = favId;
	}
	public int getBookId()
	{
		return bookId;
	}
	public void setBookId(int bookId)
	{
		this.bookId = bookId;
	}
	public String getBookName()
	{
		return BookName;
	}
	public void setBookName(String bookName)
	{
		BookName = bookName;
	}
	public String getUserName() 
	{
		return userName;
	}
	public void setUserName(String userName)
	{
		this.userName = userName;
	}
}
