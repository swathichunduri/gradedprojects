package com.MagicOfBooks.model;

import javax.persistence.*;

@Entity
@Table(name="admins")//It is the name of table in database
public class Admin 
{
	//attributes of this class
	@Id//represents that the attribute is primary key.
	@GeneratedValue(generator="admin_seq",strategy=GenerationType.SEQUENCE)
	@SequenceGenerator(name="admin_seq",sequenceName="admin_seq",initialValue=1,allocationSize=1)
	//automatically gives the unique value.
	private int adminId;
	private String adminName;
	private String password;
	private String emailId;
	private String phoneNumber;
	//getters and setters of  class attributes
	public int getAdminId()
	{
		return adminId;
	}
	public void setAdminId(int adminId) {
		this.adminId = adminId;
	}
	public String getAdminName() 
	{
		return adminName;
	}
	public void setAdminName(String adminName) 
	{
		this.adminName = adminName;
	}
	public String getPassword() 
	{
		return password;
	}
	public void setPassword(String password) 
	{
		this.password = password;
	}
	public String getEmailId()
	{
		return emailId;
	}
	public void setEmailId(String emailId) 
	{
		this.emailId = emailId;
	}
	public String getPhoneNumber() 
	{
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}
	
}
