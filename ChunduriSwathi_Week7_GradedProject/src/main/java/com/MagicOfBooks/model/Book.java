package com.MagicOfBooks.model;

import javax.persistence.*;

@Entity
@Table(name="books")//name of the table in database
public class Book 
{
	//attributes of Book class
	@Id//represents that the attribute is primary key.
	@GeneratedValue(generator="books_seq",strategy=GenerationType.SEQUENCE)
	@SequenceGenerator(name="books_seq",sequenceName="book_seq",initialValue=1,allocationSize=1)
	//automatically generates the value.
	private int bookId;
	private String bookName;
	private String authorName;
	private String publication;
	private int price;
	//getters and setters of Book class attributes.
	public int getBookId() 
	{
		return bookId;
	}
	public void setBookId(int bookId) 
	{
		this.bookId = bookId;
	}
	public String getBookName() 
	{
		return bookName;
	}
	public void setBookName(String bookName)
	{
		this.bookName = bookName;
	}
	public String getAuthorName() 
	{
		return authorName;
	}
	public void setAuthorName(String authorName) 
	{
		this.authorName = authorName;
	}
	public String getPublication()
	{
		return publication;
	}
	public void setPublication(String publication) 
	{
		this.publication = publication;
	}
	public int getPrice() 
	{
		return price;
	}
	public void setPrice(int price) 
	{
		this.price = price;
	}
}
