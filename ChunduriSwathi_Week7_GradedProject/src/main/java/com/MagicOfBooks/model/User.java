package com.MagicOfBooks.model;

import javax.persistence.*;

@Entity
@Table(name="users")//name of the table in database
public class User 
{
	//attributes of user class
	@Id//represents that attribute is primary key
	@GeneratedValue(generator="user_seq",strategy=GenerationType.SEQUENCE)
	@SequenceGenerator(name="user_seq",sequenceName="user_seq",initialValue=1,allocationSize=1)
	//automatically generates the value.
	private int userId;
	private String userName;
	private String password;
	private String emailId;
	private String phoneNumber;
	//getters and setters of user class attributes.
	public int getUserId()
	{
		return userId;
	}
	public void setUserId(int userId) 
	{
		this.userId = userId;
	}
	public String getUserName() 
	{
		return userName;
	}
	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	public String getPassword() 
	{
		return password;
	}
	public void setPassword(String password)
	{
		this.password = password;
	}
	public String getEmailId()
	{
		return emailId;
	}
	public void setEmailId(String emailId)
	{
		this.emailId = emailId;
	}
	public String getPhoneNumber()
	{
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) 
	{
		this.phoneNumber = phoneNumber;
	}
	
}
