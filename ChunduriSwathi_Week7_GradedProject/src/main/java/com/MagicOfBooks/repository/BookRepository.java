package com.MagicOfBooks.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.MagicOfBooks.model.Book;

@Repository
//gives the predefined functions of JpaRepository
public interface BookRepository extends JpaRepository<Book,Integer>
{
	//Lists the book with the given bookId.
	public List<Book> findByBookId(int bookId);

	//Lists the books with the given author name.
	public List<Book> findByAuthorName(String authorName);
	
	//Lists the books based on the given book name.
	public List<Book> findByBookName(String bookName);
	
	//lists the books based on the publication
	public List<Book> findByPublication(String publication);

	//lists the books above the given price 
	@Query("select book from Book book where price>?1")
	public List<Book> getBookByPrice(int price);

	//Lists the books in ascending order according to the price.
	public List<Book> findByOrderByPriceAsc();

	

}
