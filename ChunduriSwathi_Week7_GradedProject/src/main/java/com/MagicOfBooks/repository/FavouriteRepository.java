package com.MagicOfBooks.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.MagicOfBooks.model.Favourite;

@Repository
//here this repository can use predefined functions of JpaRepository.
public interface FavouriteRepository extends JpaRepository<Favourite,Integer> 
{

}
