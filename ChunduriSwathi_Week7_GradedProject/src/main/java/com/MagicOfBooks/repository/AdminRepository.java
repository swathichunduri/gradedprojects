package com.MagicOfBooks.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.MagicOfBooks.model.Admin;

@Repository
// gives predefined functions of JpaRepository.
public interface AdminRepository extends JpaRepository<Admin,Integer>
{

}
