package com.MagicOfBooks.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.MagicOfBooks.model.User;

@Repository
//Repository will give some predefined functions.
public interface UserRepository extends JpaRepository<User,Integer>
{
	
}
