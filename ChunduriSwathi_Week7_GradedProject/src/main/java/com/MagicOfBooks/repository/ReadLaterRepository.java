package com.MagicOfBooks.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.MagicOfBooks.model.ReadLater;

@Repository
//here this repository can use predefined functions of JpaRepository
public interface ReadLaterRepository extends JpaRepository<ReadLater,Integer>
{

}
