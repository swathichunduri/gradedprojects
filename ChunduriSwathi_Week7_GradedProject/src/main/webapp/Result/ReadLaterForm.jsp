<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Magic Of Books</title>
</head>
<body>
	<div align="center">
		<h1 style="color: Green">MAGIC OF BOOKS</h1>
		<h2 style="color: Orange">FORM</h2>
		<form:form method="POST" action="saveToReadLater"
			modelAttribute="rlater">
			<table>
				<tr>
					<td><form:hidden path="readId" /></td>
				</tr>
				<tr>
					<td><form:hidden path="bookId" /></td>
				</tr>
				<tr>
					<td><form:hidden path="BookName" /></td>
				</tr>
				<tr>
					<td><form:label path="userName">Please Enter your Name :</form:label></td>
					<td><form:input path="userName" /></td>
				</tr>
				<tr>
					<td align="center" colspan="2"><input type="submit"
						onclick="alert('Book added to your Read Later Successfully')"
						value="ADD" /></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>