<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Magic Of Books</title>
</head>
<body>
	<div align="center">
		<h1 style="color: green">ADMIN LOGIN FORM</h1>
		<form:form method="POST" action="CheckAdmin" modelAttribute="admin1">
			<table>
				<tr>
					<td><form:label path="adminName">Enter Admin Name : </form:label></td>
					<td><form:input path="adminName" /></td>
				</tr>
				<tr>
					<td><form:label path="password">Enter Password : </form:label></td>
					<td><form:input path="password" /></td>
				</tr>
				<tr>
					<td align="center" colspan="2"><input type="submit"
						value="Login" /></td>
				</tr>
			</table>
		</form:form>
		<p>
			If you don't have an account,<br> <br>click here to
			register<br> <br> <a href="AdminRegister">RegisterHere</a>
		</p>
	</div>
</body>
</html>