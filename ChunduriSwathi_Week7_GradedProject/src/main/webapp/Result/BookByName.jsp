<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Magic Of Books</title>
</head>
<body>
	<div align="center">
		<h1 style="color: Green">MAGIC OF BOOKS</h1>
		<h2 style="color: Orange">BOOK SEARCH USING BOOK NAME</h2>
		<form:form method="POST" action="GetSearchBookByName"
			modelAttribute="obj2">
			<table>
				<tr>
					<td><form:label path="bookName">Enter Book Name : </form:label></td>
					<td><form:input path="bookName" /><br></td>
				</tr>

				<tr>
					<td align="center" colspan="2"><input type="submit"
						value="SEARCH" /></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>