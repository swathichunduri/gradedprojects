<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Magic Of Books</title>
</head>
<body>
	<div align="center">
		<h1 style="color: Green">MAGIC OF BOOKS</h1>
		<h2 style="color: Orange">DASHBOARD</h2>
		<h3>
			<a href="NewBook">AddBook</a>
		</h3>
		<h3>
			<a href="RetrieveBooks">RetrieveBooks</a>
		</h3>
		<h3>
			<a href="SearchById">SearchBookUsingId</a>
		</h3>
		<h3>
			<a href="SearchByAuthor">SearchBookUsingAuthorName</a>
		</h3>
		<a href="logout">Logout</a>
	</div>
</body>
</html>