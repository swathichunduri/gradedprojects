<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Magic Of Books</title>
</head>
<body>
	<div align="center">
		<h1 style="color: Green">MAGIC OF BOOKS</h1>
		<h2 style="color: Orange">BOOK INSERT FORM</h2>
		<form:form method="POST" action="InsertBook" modelAttribute="book1">
			<table>
				<tr>
					<td><form:label path="bookName">Enter Book Name : </form:label></td>
					<td><form:input path="bookName" /></td>
				</tr>
				<tr>
					<td><form:label path="authorName">Enter Author Name : </form:label></td>
					<td><form:input path="authorName" /></td>
				</tr>
				<tr>
					<td><form:label path="publication">Enter Publication : </form:label></td>
					<td><form:input path="publication" /></td>
				</tr>
				<tr>
					<td><form:label path="price">Enter Price : </form:label></td>
					<td><form:input path="price" /></td>
				</tr>
				<tr>
					<td align="center" colspan="2"><input type="submit"
						value="ADD" /></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>