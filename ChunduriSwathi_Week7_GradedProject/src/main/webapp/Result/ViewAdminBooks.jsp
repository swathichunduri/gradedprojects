<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Magic Of Books</title>
</head>
<body>
	<div align="center">
		<h1 style="color: Green">MAGIC OF BOOKS</h1>
		<h2 style="color: Orange">BOOKS</h2>
		<h3>
			<a href="AdminDashboard">Dashboard</a>
		</h3>
		<h3>
			<a href="NewBook">AddBook</a>
		</h3>
		<table border="1">
			<th style="color: Red">SNO</th>
			<th style="color: Red">BOOK ID</th>
			<th style="color: Red">BOOK NAME</th>
			<th style="color: Red">AUTHOR NAME</th>
			<th style="color: Red">PUBLICATION</th>
			<th style="color: Red">PRICE</th>
			<th style="color: Red">ACTIONS</th>
			<c:forEach var="book" items="${bookslist}" varStatus="status">
				<tr>
					<td>${status.index + 1}</td>
					<td>${book.bookId}</td>
					<td>${book.bookName}</td>
					<td>${book.authorName}</td>
					<td>${book.publication}</td>
					<td>${book.price}</td>
					<td><a href="EditBook?bookId=${book.bookId}">Edit</a>
						&nbsp;&nbsp;&nbsp;&nbsp; <a
						href="DeleteBook?bookId=${book.bookId}">Delete</a></td>

				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>