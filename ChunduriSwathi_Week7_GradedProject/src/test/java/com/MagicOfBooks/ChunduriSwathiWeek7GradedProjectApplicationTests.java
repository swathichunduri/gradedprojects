package com.MagicOfBooks;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import com.MagicOfBooks.service.LoginBusiness;
import com.MagicOfBooks.service.LoginService;

@SpringBootTest
class ChunduriSwathiWeek7GradedProjectApplicationTests {

	@Test
	void contextLoads() {
	}
	@Test
	public void testUsingMocks()
	{
		LoginService service = mock(LoginService.class);
		List<String> list=Arrays.asList("user1","user2");
		when(service.getUserDetails("dummy")).thenReturn(list);
		LoginBusiness business =new LoginBusiness(service);
		List<String> alltd=business.getUsers("dummy");
		assertEquals(1,alltd.size());
	}
}
