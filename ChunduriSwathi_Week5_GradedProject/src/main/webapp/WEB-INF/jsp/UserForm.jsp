<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>New User</title>
</head>
<body>
    <div align="center">
        <h1>ADD USER</h1>
        <form:form action="saveUser" method="post" modelAttribute="u1">
        <table>
            <tr>
                <td>User Id:</td>
                <td><form:input path="userId" /></td>
            </tr>
            <tr>
                <td>User Name:</td>
                <td><form:input path="userName" /></td>
            </tr>
            <tr>
                <td>Phone Number:</td>
                <td><form:input path="phoneNumber" /></td>
            </tr>
            <tr>
                <td>EmailID:</td>
                <td><form:input path="emailId" /></td>
            </tr>
            <tr>
                <td colspan="2" align="center"><input type="submit" value="Save"></td>
            </tr>
        </table>
        </form:form>
    </div>
</body>
</html>