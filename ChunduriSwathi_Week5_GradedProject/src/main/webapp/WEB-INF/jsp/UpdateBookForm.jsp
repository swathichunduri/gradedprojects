<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Book Updation</title>
</head>
<body>
<div align="center">
<h1>Update Book</h1>
<form:form method="POST" action="updateBook" modelAttribute="edit_book">   
<table >   
<tr>
<td></td>   
<td><form:hidden  path="bookId" /></td>
</tr>  
<tr>   
<td>Book Name : </td>  
<td><form:input path="bookName"  /></td>
</tr>   
<tr>   
<td>Description :</td>   
<td><form:input path="description" /></td>
</tr>  
<tr>   
<td>Author Name:</td>   
<td><form:input path="authorName" /></td>
</tr>
<tr>   
<td>Price:</td>   
<td><form:input path="price" /></td>
</tr>       
<tr>    
<td><input type="submit" value="Update" /></td>   
</tr>   
</table>   
</form:form>
</div>
</body>
</html>  