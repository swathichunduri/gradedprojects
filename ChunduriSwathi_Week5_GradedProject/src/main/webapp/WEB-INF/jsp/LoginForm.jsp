<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri = "http://www.springframework.org/tags/form"
prefix = "form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div align="center">
<h1 style="color:Orange">LOGIN</h1>
<form:form method = "POST" action = "search" modelAttribute="a2">
<table>
            <tr>
               <td><form:label path = "adminId">Admin ID</form:label></td>
               <td><form:input path = "adminId" /></td>
            </tr>
            <tr>
               <td><form:label path = "adminName">Admin Name</form:label></td>
               <td><form:input path = "adminName" /></td>
            </tr>
            <tr>
               <td><form:label path = "password">Password</form:label></td>
               <td><form:input path = "password" /></td>
            </tr>
            <tr>
               <td colspan = "2">
                  <input type = "submit" value = "Login"/>
               </td>
            </tr>
         </table>  
      </form:form>
</div>
</body>
</html>