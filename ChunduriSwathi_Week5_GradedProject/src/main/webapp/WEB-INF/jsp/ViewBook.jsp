<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Books</title>
    </head>
    <body>
        <div align="center">
            <h1>BOOKS</h1>
            <h3><a href="newBook">ADD BOOK</a></h3>
            <table border="1">
            	<th>SNO</th>
                <th>BOOKID</th>
                <th>BOOKNAME</th>
                <th>DESCRIPTION</th>
                <th>AUTHOR NAME</th>
                <th>PRICE</th>
                <th>ACTION</th>
                 
                <c:forEach var="book" items="${listBook}" varStatus="status">
                <tr>
                    <td>${status.index + 1}</td>
                    <td>${book.bookId}</td>
                    <td>${book.bookName}</td>
                    <td>${book.description}</td>
                    <td>${book.authorName}</td>
                    <td>${book.price}</td>
                    <td>
                        <a href="editBook?bookId=${book.bookId}">Edit</a>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="deleteBook?bookId=${book.bookId}">Delete</a>
                    </td>
                             
                </tr>
                </c:forEach>             
            </table>
            <a href="logout">LOGOUT</a>
        </div>
    </body>
</html>