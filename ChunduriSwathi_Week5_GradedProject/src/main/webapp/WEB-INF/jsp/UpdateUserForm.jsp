<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>User Updation</title>
</head>
<body>
<div align="center">
<h1>Update User</h1>
<form:form method="POST" action="updateUser" modelAttribute="edit_user">   
<table >   
<tr>
<td></td>   
<td><form:hidden  path="UserId" /></td>
</tr>  
<tr>   
<td>User Name : </td>  
<td><form:input path="userName"  /></td>
</tr>   
<tr>   
<td>Phone Number :</td>   
<td><form:input path="phoneNumber" /></td>
</tr>  
<tr>   
<td>EmailId</td>   
<td><form:input path="emailId" /></td>
</tr> 
<tr> 
<td><input type="submit" value="Update" /></td>   
</tr>   
</table>   
</form:form>
</div>
</body>
</html>  