<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>New Book</title>
</head>
<body>
    <div align="center">
        <h1>ADD Book</h1>
        <form:form action="saveBook" method="post" modelAttribute="b1">
        <table>
        	<tr>
                <td>Book Id:</td>
                <td><form:input path="bookId" /></td>
            </tr>
            
            <tr>
                <td>Book Name:</td>
                <td><form:input path="bookName" /></td>
            </tr>
            <tr>
                <td>Description:</td>
                <td><form:input path="description" /></td>
            </tr>
            <tr>
                <td>Author Name:</td>
                <td><form:input path="authorName" /></td>
            </tr>
            <tr>
                <td>Price:</td>
                <td><form:input path="price" /></td>
            </tr>
            <tr>
                <td colspan="2" align="center"><input type="submit" value="Save"></td>
            </tr>
        </table>
        </form:form>
    </div>
</body>
</html>