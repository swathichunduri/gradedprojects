package com.controller.pojo;

public class User 
{
	//attributes of user class.
	private int userId;
	private String userName;
	private String phoneNumber;
	private String emailId;
	//Getters and Setters of user class attributes.
	public int getUserId() 
	{
		return userId;
	}
	public void setUserId(int userId) 
	{		
		this.userId = userId;
	}
	public String getUserName() 
	{
		return userName;
	}
	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	public String getPhoneNumber() 
	{
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}
	public String getEmailId() 
	{
		return emailId;
	}
	public void setEmailId(String emailId) 
	{
		this.emailId = emailId;
	}
}
