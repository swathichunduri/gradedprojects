package com.controller.pojo;

public class Admin 
{
	//attributes of Admin class.
	private int adminId;
	private String adminName;
	private String password;
	//Getters and Setters of Admin class attributes. 
	public int getAdminId() 
	{
		return adminId;
	}
	public void setAdminId(int adminId) 
	{
		this.adminId = adminId;
	}
	public String getAdminName() 
	{
		return adminName;
	}
	public void setAdminName(String adminName)
	{
		this.adminName = adminName;
	}
	public String getPassword() 
	{
		return password;
	}
	public void setPassword(String password) 
	{
		this.password = password;
	}
}
