package com.controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.controller.dao.BookDAOImpl;
import com.controller.pojo.Book;

@Controller
public class BookController 
{
	@Autowired
	BookDAOImpl bdao;
	//User goes to Book page when he/She clicks on BookOperations
	@RequestMapping("/BookOperations")
	public String getBook()
	{
		return "Book";
	}
	//here a new book form  will be displayed for adding a book 
	@RequestMapping("/newBook")
	public ModelAndView addBook()
	{
		return new ModelAndView("BookForm","b1",new Book());
	}
	@RequestMapping("/saveBook")
	public ModelAndView saveBook(@ModelAttribute ("b1") Book b1)
	{
		bdao.save(b1);
		//redirects to main page after registration
		return new ModelAndView("redirect:/RetrieveBooks");
	}
	@RequestMapping(value="/RetrieveBooks")
	public ModelAndView getBookList(ModelAndView model) throws IOException
	{
		List<Book> listBook=bdao.list();
		model.addObject("listBook", listBook);
		model.setViewName("ViewBook");
		return model;
	}
	//deletes a book by clicking the delete 
	@RequestMapping("/deleteBook")
	public ModelAndView deleteBook(HttpServletRequest request) {
	    int bookid= Integer.parseInt(request.getParameter("bookId"));
	    bdao.delete(bookid);
	    return new ModelAndView("redirect:/RetrieveBooks");
	    //redirects to books table 
	}
	//edit will display a form to edit update the changes
	@RequestMapping("/editBook")
	public ModelAndView editBook(HttpServletRequest request)
	{
		int bookid=Integer.parseInt(request.getParameter("bookId"));
		Book book=bdao.get(bookid);
		ModelAndView model=new ModelAndView("UpdateBookForm");
		model.addObject("edit_book", book);
		return model;
	}
	@RequestMapping("/updateBook")
	public ModelAndView UpdateBook(@ModelAttribute ("edit_book") Book b1)
	{
		bdao.update(b1);
		return new ModelAndView("redirect:/RetrieveBooks");
		//again comes to books table.
	}
}
