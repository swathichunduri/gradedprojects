package com.controller;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.controller.dao.UserDAOImpl;
import com.controller.pojo.User;
import java.util.*;
import javax.servlet.http.HttpServletRequest;

@Controller
public class UserController 
{
	@Autowired
	UserDAOImpl udao;
	//UserOperations can be done here
	@RequestMapping("/UserOperations")
	public String getUserForm()
	{
		return "User";
	}
	//a new user can be added here.
	@RequestMapping("/newUser")
	public ModelAndView addUser()
	{
		return new ModelAndView("UserForm","u1",new User());
	}
	@RequestMapping("/saveUser")
	public ModelAndView saveUser(@ModelAttribute ("u1") User u1)
	{
		udao.save(u1);
		return new ModelAndView("redirect:/RetrieveUsers");
	}
	//can retrieve the user details
	@RequestMapping(value="/RetrieveUsers")
	public ModelAndView getUserList(ModelAndView model) throws IOException
	{
		List<User> listUser=udao.list();
		model.addObject("listUser", listUser);
		model.setViewName("ViewUser");
		return model;
	}
	//delete the user.
	@RequestMapping("/deleteUser")
	public ModelAndView deleteUser(HttpServletRequest request) 
	{
	    int userid= Integer.parseInt(request.getParameter("userId"));
	    udao.delete(userid);
	    return new ModelAndView("redirect:/RetrieveUsers");
	}
	//Edit the user information
	@RequestMapping("/editUser")
	public ModelAndView editUser(HttpServletRequest request)
	{
		int userid=Integer.parseInt(request.getParameter("userId"));
		User user=udao.get(userid);
		ModelAndView model=new ModelAndView("UpdateUserForm");
		model.addObject("edit_user", user);
		return model;
	}
	@RequestMapping("/updateUser")
	public ModelAndView UpdateBook(@ModelAttribute ("edit_user") User u1)
	{
		udao.update(u1);
		return new ModelAndView("redirect:/RetrieveUsers");
	}
}
