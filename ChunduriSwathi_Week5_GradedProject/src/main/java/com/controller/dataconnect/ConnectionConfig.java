package com.controller.dataconnect;
import javax.sql.DataSource;
import com.controller.dao.AdminDAO;
import com.controller.dao.BookDAOImpl;
import com.controller.dao.UserDAOImpl;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
@Configuration
public class ConnectionConfig {
    DriverManagerDataSource dataSource;
    JdbcTemplate jdbcTemplate;
     AdminDAO adao;
     UserDAOImpl udao;
     BookDAOImpl bdao;

    @Bean
    public DataSource dataSource()
    {
    	//connecting the database
        dataSource=    new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl
        ("jdbc:mysql://localhost:3306/Magic_of_books");
        dataSource.setUsername("root");
        dataSource.setPassword("Swathi@123#");
        return dataSource;
    }
    @Bean
    public JdbcTemplate jdbcTemplate()
    {
        jdbcTemplate=new JdbcTemplate();
        jdbcTemplate.setDataSource(dataSource());
        return jdbcTemplate;

    }
    @Bean
    public AdminDAO adminDAO() 
    {
    	//Connecting admindaoimpl with the database.
        adao=new AdminDAO();
        adao.setJdbctemplate(jdbcTemplate());
        return adao;
    }
    @Bean UserDAOImpl userDAOimpl()
    {
    	//Connecting userdaoimpl with the database 
    	udao=new UserDAOImpl();
    	udao.setJdbctemplate(jdbcTemplate());
		return udao;
    }
    @Bean BookDAOImpl bookDAOimpl()
    {
    	//connecting bookdaoimpl with the database
    	bdao=new BookDAOImpl();
    	bdao.setJdbctemplate(jdbcTemplate());
		return bdao;
    }
}