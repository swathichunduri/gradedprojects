package com.controller.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import com.controller.pojo.User;

public class UserDAOImpl implements UserDAO
{
JdbcTemplate jdbcTemplate;
	

	public void setJdbctemplate(JdbcTemplate jdbctemplate) 
	{
		this.jdbcTemplate = jdbctemplate;
	}
	//Parameterized Constructor
	public UserDAOImpl(DataSource dataSource)
	{
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	//Constructor
	public UserDAOImpl()
	{
		
	}
	@Override
	public void save(User user) 
	{
		//Insert into save in the database
		String sql ="INSERT INTO users VALUES (?,?, ?, ?)";
	    jdbcTemplate.update(sql,user.getUserId(),user.getUserName(),user.getPhoneNumber(),user.getEmailId());
	}
		
	@Override
	public void delete(int uid) 
	{
		//delete from the database.
		String sql = "DELETE FROM users WHERE userid=?";
	    jdbcTemplate.update(sql, uid);
	}
	
	@Override
	public List<User> list()
	{
		//retrieves the users in the database
		String sql = "SELECT * FROM users";
	    List<User> listBook = jdbcTemplate.query(sql, new RowMapper<User>() 
	    {
	    	@Override
	        public User mapRow(ResultSet rs, int rowNum) throws SQLException
	    	{
	            User user1= new User();
	            user1.setUserId(rs.getInt("userid"));
	            user1.setUserName(rs.getString("username"));
	            user1.setPhoneNumber(rs.getString("phoneno"));
	            user1.setEmailId(rs.getString("emailid"));
	            return user1;
	        }
	    });
	    return listBook;
	}
	
	@Override
	public User get(int user_id) 
	{
		//retrieves a single user from database
		String sql = "SELECT * FROM users WHERE userid=" + user_id;
	    return jdbcTemplate.query(sql, new ResultSetExtractor<User>() 
	    {	
	        @Override
	        public User extractData(ResultSet rs) throws SQLException,DataAccessException 
	        {
	            if (rs.next()) 
	            {
	            	User user1= new User();
	            	user1.setUserId(rs.getInt("userid"));
		            user1.setUserName(rs.getString("username"));
		            user1.setPhoneNumber(rs.getString("phoneno"));
		            user1.setEmailId(rs.getString("emailid"));
		            return user1;
	          }
	          return null;
	        }
	 
	    });
		
	}
	
	@Override
	public void update(User u1) 
	{
		//updates the user information.
		String sql="update users set username=?,phoneno=?,emailid=? where userid=?";
		jdbcTemplate.update(sql,u1.getUserName(),u1.getPhoneNumber(),u1.getEmailId(),u1.getUserId());
	}

}
