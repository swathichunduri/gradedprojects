package com.controller.dao;

import java.util.List;

import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import com.controller.pojo.Admin;

public class AdminDAO 
{
	 JdbcTemplate jdbcTemplate;
	
	 //parameterized constructor	
	public void setJdbctemplate(JdbcTemplate jdbctemplate) {
		this.jdbcTemplate = jdbctemplate;
	}
	public AdminDAO(DataSource dataSource)
	{
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	//constructor
	public AdminDAO()
	{
		
	}

	public void insert(Admin admin) 
	{
		//inserting values into  database.
		String query1="insert into admin values(?,?,?)";
        jdbcTemplate.update(query1,admin.getAdminId(),admin.getAdminName(),admin.getPassword());
	}
	/*public boolean search(int admin_id)
	{
		//checks whether admin id in the database.
		String query="select adminid from admin where adminid="+admin_id+"";
		int result=jdbcTemplate.update(query);
		if(result==1)
		{
			return true;
		}
		return false;
	}*/
	
}
