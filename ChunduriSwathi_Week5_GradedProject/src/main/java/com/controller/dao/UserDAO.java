package com.controller.dao;

import java.util.List;

import com.controller.pojo.User;

public interface UserDAO 
{
	//functions of userDAO
	public void save(User user);
    
	public void delete(int userid); 
     
    public User get(int userId);
     
    public List<User> list();
    
    public void update(User userId);
}
