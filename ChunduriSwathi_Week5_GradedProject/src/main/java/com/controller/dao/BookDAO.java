package com.controller.dao;
 
import java.util.List;
import com.controller.pojo.Book;
 
public interface BookDAO 
{
     //functions of BookDAO
    public void save(Book book);
     
    public void delete(int bookId);
     
    public Book get(int bookId);
     
    public List<Book> list();
    
    public void update(Book bookId);
}