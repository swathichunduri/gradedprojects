package com.controller.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import com.controller.pojo.Book;

public class BookDAOImpl implements BookDAO
{
JdbcTemplate jdbcTemplate;
	

	public void setJdbctemplate(JdbcTemplate jdbctemplate) 
	{
		this.jdbcTemplate = jdbctemplate;
	}
	public BookDAOImpl(DataSource dataSource)
	{
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	public BookDAOImpl()
	{
		
	}
	
	@Override
	public void save(Book book) 
	{
		//inserts value into database
		String sql ="INSERT INTO books VALUES (?,?, ?, ?, ?)";
	    jdbcTemplate.update(sql,book.getBookId(), book.getBookName(), book.getDescription(),book.getAuthorName(), book.getPrice());
	}
		
	@Override
	public void delete(int bid) 
	{
		//deletes book from database
		String sql = "DELETE FROM books WHERE bookid=?";
	    jdbcTemplate.update(sql, bid);
	}
	
	@Override
	public List<Book> list()
	{
		//retrieves books from database.
		String sql = "SELECT * FROM books";
	    List<Book> listBook = jdbcTemplate.query(sql, new RowMapper<Book>() 
	    {
	    	@Override
	        public Book mapRow(ResultSet rs, int rowNum) throws SQLException
	    	{
	            Book aBook= new Book();
	            aBook.setBookId(rs.getInt("bookid"));
	            aBook.setBookName(rs.getString("bookname"));
	            aBook.setDescription(rs.getString("description"));
	            aBook.setAuthorName(rs.getString("authorname"));
	            aBook.setPrice(rs.getInt("price"));
	            return aBook;
	        }
	 
	    });
	    return listBook;
	}
	
	@Override
	public Book get(int book_id) 
	{
		//gets a single book information
		String sql = "SELECT * FROM books WHERE bookid=" + book_id;
	    return jdbcTemplate.query(sql, new ResultSetExtractor<Book>() 
	    {	
	        @Override
	        public Book extractData(ResultSet rs) throws SQLException,DataAccessException 
	        {
	            if (rs.next()) 
	            {
	            	Book aBook= new Book();
	           	 	aBook.setBookId(rs.getInt("bookid"));
		            aBook.setBookName(rs.getString("bookname"));
		            aBook.setDescription(rs.getString("description"));
		            aBook.setAuthorName(rs.getString("authorname"));
		            aBook.setPrice(rs.getInt("price"));
		            return aBook;
	          }
	          return null;
	        }
	 
	    });
		
	}
	
	@Override
	public void update(Book b1) 
	{
		//updates the book information
		String sql="update books set bookname=?,description=?,authorname=?,price=? where bookid=?";
		jdbcTemplate.update(sql,b1.getBookName(),b1.getDescription(),b1.getAuthorName(),b1.getPrice(),b1.getBookId());
		
	}
}
