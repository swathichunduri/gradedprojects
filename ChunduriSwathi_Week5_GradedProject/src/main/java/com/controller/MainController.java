package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.controller.dao.AdminDAO;
import com.controller.pojo.Admin;


@Controller
public class MainController 
{
	@Autowired
	AdminDAO admindao;
	// automatically comes to welcome page
	@RequestMapping("/")
	public String getPage()
	{
		return "welcome";
	}
	//registration will be done here
	 @RequestMapping("/Register")
	 public ModelAndView AdminRegister()
	 {
		 return new ModelAndView("RegisterForm","a1",new Admin());
	 }
	 //saves the values in database by calling save function
	 @RequestMapping(value="/save" ,method=RequestMethod.POST)    
	    public ModelAndView save(@ModelAttribute("a1") Admin a1)
	 {    
	        admindao.insert(a1); 
	        return new ModelAndView("redirect:/Main");  
	 }
	 @RequestMapping("/Main")
	 public ModelAndView getMainPage() 
	 {
		 return new ModelAndView("MainPage");
	 }
	 // can login directly.
	@RequestMapping("/Login")
	public ModelAndView adminlogin()
	{
		return new ModelAndView("LoginForm","a2",new Admin());
	}
	@RequestMapping(value="/search" )    
    public ModelAndView search(@ModelAttribute("a2") Admin a2)
	{    
		int id=a2.getAdminId();
		String name=a2.getAdminName();
		String pwd=a2.getPassword();
        return new ModelAndView("redirect:/Main");  
	}
	//we can signout from the application
	 
	@RequestMapping("/logout")
	public ModelAndView signout()
	{
		return new ModelAndView("redirect:/");
	}
}
