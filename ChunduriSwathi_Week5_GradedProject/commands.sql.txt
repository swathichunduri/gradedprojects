/*Creation of database*/
create database magic_of_books;


/*using the database*/
use magic_of_books;


/*creating books table*/
create table books
(
	bookid int primary key,
	bookname varchar(50),
	description varchar(100),
	authorname varchar(50),
	price int
);


/*creating users table*/
create table users
(
	userid int primary key,
	username varchar(50),
	phoneno varchar(20),
	emailid varchar(50)
);


/*creating  admin  table*/
create table admin
(
	adminid int primary key,
	adminname varchar(50),
	password varchar(50)
);


/*Inserting values into admin*/
insert into admin values(101,"admin1","admin1");


/*Inserting values into users*/
insert into users values(1,"user1","998887777","user1@gamil.com");


/*Deleting values from users*/
delete from users where userid=1;


/*Retreiving the values from users table*/
select * from users;


/*Retreiving a single value from users table */
select * from users where userid=1;


*Inserting values into books*/
insert into books values(1,"book1","book1","John",89);


/*Deleting values from books*/
delete from users where bookid=1;


/*Retreiving the values from books table*/
select * from books;


/*Retreiving a single value from books table */
select * from books where bookid=1;
