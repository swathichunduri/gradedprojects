<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Magic Of Books</title>
</head>
<body>
	<div align="center">
		<h1 style="color: Green">MAGIC OF BOOKS</h1>
		<p>Dear User,</p>
		<p>Please Register first if you are a new User.</p>
		<p>After Registering, please login to the application to get the
			token.</p>
		<h3>
			<a href="JWTRegisterForm">JWT Register</a>
		</h3>
		<h3>
			<a href="/JWTLoginForm">JWT Login</a>
		</h3>
	</div>
</body>
</html>