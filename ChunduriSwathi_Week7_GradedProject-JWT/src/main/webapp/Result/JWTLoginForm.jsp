<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Magic Of Books</title>
</head>
<body>
	<div align="center">
		<h1 style="color: Orange">JWT LOGIN FORM</h1>
		<form:form method="POST" action="Authenticate"
			modelAttribute="userobj2">
			<table>

				<tr>
					<td><form:label path="username">User Name</form:label></td>
					<td><form:input path="username" /></td>
				</tr>
				<tr>
					<td><form:label path="password">Password</form:label></td>
					<td><form:input path="password" /></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="submit"
						value="login" /></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>