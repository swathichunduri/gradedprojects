package com.MagicOfBooks.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.MagicOfBooks.model.UserDTO;

@Controller
public class MainController 
{
	//http://localhost:8081/ is used to access the mainpage 
	@RequestMapping("/")
	public String getJWT()
	{
		return "JWTWelcome";
	}
	@RequestMapping("/JWTRegisterForm")
	public ModelAndView getJWTRegister()
	{
		//registration page can be viewed here
		return new ModelAndView("JWTRegisterForm","userobj",new UserDTO());
	}
	@RequestMapping("/JWTLoginForm")
	public ModelAndView getJWTLogin()
	{
		//login page can be viewed here
		return new ModelAndView("JWTLoginForm","userobj2",new UserDTO());
	}
	@RequestMapping("/MagicOfBooks")
	public String getWelcomePage()
	{
		return "Welcome";
	}
}
