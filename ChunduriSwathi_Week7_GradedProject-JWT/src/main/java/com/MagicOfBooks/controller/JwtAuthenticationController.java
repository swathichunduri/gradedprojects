package com.MagicOfBooks.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.MagicOfBooks.config.JwtTokenUtil;
import com.MagicOfBooks.model.JwtRequest;
import com.MagicOfBooks.model.JwtResponse;
import com.MagicOfBooks.model.UserDTO;
import com.MagicOfBooks.service.JwtUserDetailsService;

import io.jsonwebtoken.lang.Objects;

@RestController
@CrossOrigin
public class JwtAuthenticationController 
{


	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUserDetailsService userDetailsService;

	@RequestMapping(value = "/Authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@ModelAttribute("userobj2") JwtRequest authenticationRequest) throws Exception {
		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

		final UserDetails userDetails = userDetailsService
				.loadUserByUsername(authenticationRequest.getUsername());

		final String token = jwtTokenUtil.generateToken(userDetails);

		return ResponseEntity.ok(new JwtResponse(token));
	}
	@RequestMapping(value = "/JWTregister", method = RequestMethod.POST)
	public ResponseEntity<?> saveUser(@ModelAttribute ("userobj") UserDTO user) throws Exception 
	{
		return ResponseEntity.ok(userDetailsService.save(user));
		
	}

	private void authenticate(String username, String password) throws Exception 
	{
		try 
		{
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} 
		catch (DisabledException e) 
		{
			throw new Exception("USER_DISABLED", e);
		} 
		catch (BadCredentialsException e) 
		{
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}


}
