package com.MagicOfBooks.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.MagicOfBooks.model.DAOUser;

public interface UserDao extends JpaRepository<DAOUser, Integer>
{
	//finds the user with the given userName..
	@Query("select user from DAOUser user where user.username=?1")
	public DAOUser findByUsername(String username);
}
