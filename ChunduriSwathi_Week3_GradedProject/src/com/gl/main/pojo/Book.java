package com.gl.main.pojo;

public class Book 
{
	//attributes of class book
	private int bookid;
	private String bookname;
	private String description;
	private String authorName;
	private int price;
	private int noOfCopies;
	private String genre;
	public Book()
	{
	}
	public Book(int i, String bookid, String bookname, String description, int price,int noOfCopies,String genre) 
	{
		
	}
	//Getters and setters of book class attributes
	public int getBookid() 
	{
		return bookid;
	}
	public void setBookid(int bookid) 
	{
		this.bookid = bookid;
	}
	public String getBookname() 
	{
		return bookname;
	}
	public void setBookname(String bookname) 
	{
		this.bookname = bookname;
	}
	public String getDescription() 
	{
		return description;
	}
	public void setDescription(String description) 
	{
		this.description = description;
	}
	public String getAuthorName() 
	{
		return authorName;
	}
	public void setAuthorName(String authorName) 
	{
		this.authorName = authorName;
	}
	public int getNoOfCopies() 
	{
		return noOfCopies;
	}
	public void setNoOfCopies(int noOfCopies) 
	{
		this.noOfCopies = noOfCopies;
	}
	public int getPrice() 
	{
		return price;
	}
	public void setPrice(int price) 
	{
		this.price = price;
	}
	public String getGenre() 
	{
		return genre;
	}
	public void setGenre(String genre) 
	{
		this.genre = genre;
	}
}
