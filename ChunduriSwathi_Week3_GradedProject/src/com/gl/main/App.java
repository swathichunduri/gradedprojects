package com.gl.main;
import java.io.IOException;
import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import com.gl.main.exceptions.NegativeNumberException;
import com.gl.service.AdminServiceImpl;
import com.gl.service.BookServiceImpl;
import com.gl.service.UserServiceImpl;
public class App extends Thread
{
	static Scanner sc;
	UserServiceImpl usi;
	AdminServiceImpl asi;
	BookServiceImpl bsi;
	public App()
	{
		sc=new Scanner(System.in);
		usi=new UserServiceImpl();
		asi=new AdminServiceImpl();
		bsi=new BookServiceImpl();
	}
	public void run()
	{
		int choice;
		do {
			System.out.println("-------------MAGIC OF BOOKS-----------");
			System.out.println("1.Admin");
			System.out.println("2.User");
			System.out.println("Enter your choice: ");
			choice=sc.nextInt();
			switch(choice)
			{
			case 1: 
				try 
				{
					adminFunctions();
				} 
				catch (NegativeNumberException e) 
				{
					System.out.println(e.getMessage());
				}
					break;
			case 2:
				try 
				{
					userFunctions();
				} 
				catch (NegativeNumberException e) 
				{
					System.out.println(e.getMessage());
				}
					break;
				
			}
		}while(choice!=0);
	}
	public boolean adminMenu()
	{
		int choice;
		do
		{
			System.out.println("--------------WELOME-------------");
			System.out.println("1.Register");
			System.out.println("2.Login");
			System.out.println("0.logout");
			System.out.println("Enter your choice ");
			choice=sc.nextInt();
			switch(choice)
			{
				case 1:
					asi.adminRegister();
				case 2:
					return asi.adminLogin(true);
			}
		}
		while(choice!=0);
		return false;
	}
	public void adminFunctions() throws NegativeNumberException
	{
		if(this.adminMenu())
		{
			int choice;
			do
			{

				System.out.println("---------------ADMIN MENU---------------");
				System.out.println("1.Add Book");
				System.out.println("2.Delete Book");
				System.out.println("3.Modify Book");
				System.out.println("4.Display all books");
				System.out.println("5.Total count of books");
				System.out.println("6.Dispaly Autobiographies");
				System.out.println("7.Arrange Books");
				System.out.println("0.Exit");
				System.out.println("Enter your choice ");
				choice=sc.nextInt();
				if(choice<0)
				{
					throw new NegativeNumberException();
				}
				switch(choice) {
					case 1:	
						bsi.addBook();
						break;
					case 2:
						bsi.deleteBook();
						break;
					case 3:
						bsi.modifyBook();
						break;
					case 4:
						bsi.displayAll();
						break;
					case 5:
						bsi.totalCountOfBooks();
						break;
					case 6:
						bsi.seeGenreBooks();
						break;
					case 7:
						bsi.arrangeBooks();
						break;
				}
			}while(choice!=0);
		}
	}

	public boolean userMenu() 
	{
		int choice;
		do
		{
			System.out.println("----------------WELCOME------------------");
			System.out.println("1.Register");
			System.out.println("2.Login");
			System.out.println("0.logout");
			System.out.println("Enter your choice ");
			choice=sc.nextInt();
			switch(choice)
			{
			case 1:
				usi.userRegister();
			case 2:
				return usi.UserLogin(true);
			}
		}while(choice!=0);
		return false;
	}
	public void userFunctions() throws NegativeNumberException
	{
		if(this.userMenu()) {
			int choice;
			do
			{
				System.out.println("--------------UserMenu----------------");
				System.out.println("1.New Books");
				System.out.println("2.select Book");
				System.out.println("3.Get Book");
				System.out.println("0.Exit");
				System.out.println("Enter your choice ");
				choice=sc.nextInt();
				if(choice<0)
				{
					throw new NegativeNumberException();
				}
				switch(choice) {
					case 1:	
						bsi.newBooks();
						break;
					case 2:
						bsi.selectBook();
						break;
					case 3:
						bsi.getBook();
						break;
				}
			}while(choice!=0);
		}
	}

	public static void main(String[] args) 
	{
		boolean append = true;
        FileHandler handler = null;
		try 
		{
			handler = new FileHandler("C:\\Users\\swathi.chunduri\\eclipse-workspace\\ChunduriSwathi_Week3_GradedProject\\src\\log.txt", append);
		} 
		catch (SecurityException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
        logger.addHandler(handler);
        logger.severe("severe message");
        logger.warning("warning message");
        logger.info("info message");
        logger.config("config message");
        logger.fine("fine message");
        logger.finer("finer message");
        logger.finest("finest message");
		App app=new App();
		app.start();
	}

}
