package com.gl.main.dao;
import java.util.*;
import com.gl.main.pojo.Book;

public interface MagicOfBooks 
{
	public void addBookDetails(Book book);
	public void deleteBookDetails(Book dbook);
	public Map<Integer, Book> displayAllBooks();
	public List<Book> totalCountOfBooks();
	public List<Book> seeGenreBooks();
	public HashMap<Integer, Book> newBooks();
	public void selectBook(Book sbook);
	public List<Book> lowToHigh();
	public List<Book> highToLow();
	public List<Book> bestSelling();
	public void modifyPrice(Book mbook);
	public HashMap<Integer, Book> getBook();
}
