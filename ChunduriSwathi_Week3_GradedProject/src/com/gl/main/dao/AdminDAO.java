package com.gl.main.dao;
import java.util.*;
import com.gl.main.pojo.Admin;

public interface AdminDAO 
{
	public void register(Admin admin);
	public List<Admin> login();
}
