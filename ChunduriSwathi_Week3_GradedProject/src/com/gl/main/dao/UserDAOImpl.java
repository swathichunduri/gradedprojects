package com.gl.main.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import com.gl.main.connect.DataConnect;
import com.gl.main.pojo.User;

public class UserDAOImpl implements UserDAO
{

	static Scanner sc;
	private Connection con1;
	private PreparedStatement stat;
	public UserDAOImpl()
	{
		con1=DataConnect.getConnect();
	}
	
	@Override
	public void register(User user) 
	{
		try
		{
			stat=con1.prepareStatement("insert into user values(?,?,?)");
			stat.setInt(1,user.getUserId());
			stat.setString(2, user.getUsername());
			stat.setString(3, user.getPassword());
			int result=stat.executeUpdate();
			if(result>0)
			{
				System.out.println("Data inserted");
			}
		}
		catch(SQLException ex)
		{
			System.out.println(ex.getMessage());
		}
	}

	@Override
	public List<User> login()
	{
		List<User> userlist=new ArrayList<User>();
		try
		{
			stat=con1.prepareStatement("select userid,password from user");
			ResultSet result=stat.executeQuery();
			while(result.next())
			{
				User u1=new User();
				u1.setUserId(result.getInt(1));
				u1.setPassword(result.getString(2));
				userlist.add(u1);
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		return userlist;
	}
}