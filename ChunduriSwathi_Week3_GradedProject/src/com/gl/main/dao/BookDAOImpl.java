package com.gl.main.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import com.gl.main.connect.DataConnect;
import com.gl.main.pojo.Book;

public class BookDAOImpl implements MagicOfBooks
{
	private Connection con1;
	private PreparedStatement stat;
	public BookDAOImpl()
	{
		con1=DataConnect.getConnect();
	}
	@Override
	public void addBookDetails(Book abook) 
	{
		try {
			stat=con1.prepareStatement("insert into book(bookid,bookname,description,authorname,price,copiesSold,Autobiography) values(?,?,?,?,?,?,?)");
			stat.setInt(1, abook.getBookid());
			stat.setString(2, abook.getBookname());
			stat.setString(3, abook.getDescription());
			stat.setString(4, abook.getAuthorName());
			stat.setInt(5, abook.getPrice());
			stat.setInt(6, abook.getNoOfCopies());
			stat.setString(7, abook.getGenre());
			int result=stat.executeUpdate();
			if(result>0)
			{
				System.out.println("Data inserted");
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteBookDetails(Book dbook) 
	{
		try 
		{
			stat=con1.prepareStatement("delete from book where bookid=?");
			stat.setInt(1, dbook.getBookid());
			int result=stat.executeUpdate();
			if(result>0)
			{
				System.out.println("Data deleted successfully");
			}
			else
			{
				System.out.println("No book with that id");
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
	}
	@Override
	public void modifyPrice(Book mbook)
	{
		try
		{
			stat=con1.prepareStatement("update book set price=? where bookid=?");
			stat.setInt(1,mbook.getPrice());
			stat.setInt(2,mbook.getBookid());
			int result=stat.executeUpdate();
			if(result>0)
			{
				System.out.println("Data updated sucessfully");
			}
			else
			{
				System.out.println("Bookid not found");
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
	}

	@Override
	public HashMap<Integer, Book> displayAllBooks() 
	{
		HashMap<Integer, Book> booklist=new HashMap<Integer,Book>();
		try {
		stat=con1.prepareStatement("select * from book");
		
			ResultSet result=stat.executeQuery();
			while(result.next())
			{
				Book b1=new Book();
				b1.setBookid(result.getInt(1));
				b1.setBookname(result.getString(2));
				b1.setDescription(result.getString(3));
				b1.setAuthorName(result.getString(4));
				b1.setPrice(result.getInt(5));
				b1.setNoOfCopies(result.getInt(6));
				b1.setGenre(result.getString(7));
				booklist.put(b1.getBookid(),b1);
				
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return booklist;
		
	}

	@Override
	public List<Book> totalCountOfBooks() 
	{
		List<Book> bookcount=new ArrayList<Book>(); 
		try 
		{
			stat=con1.prepareStatement("select copiessold,bookname from book");
			ResultSet result=stat.executeQuery();
			while(result.next())
			{
				Book b1=new Book();
				b1.setNoOfCopies(result.getInt(1));
				b1.setBookname(result.getString(2));
				bookcount.add(b1);
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
		return bookcount;
	}

	@Override
	public List<Book> seeGenreBooks() 
	{
		List<Book> genrebook=new ArrayList<Book>();
		try 
		{
			
			stat=con1.prepareStatement("select * from book where autobiography like 'y%'");
			ResultSet result=stat.executeQuery();
			while(result.next())
			{
				Book b1=new Book();
				b1.setBookname(result.getString(2));
				genrebook.add(b1);
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
		return genrebook;
		
	}

	@Override
	public List<Book> lowToHigh() 
	{
		List<Book> l2hlist=new ArrayList<Book>();
		try
		{
			stat=con1.prepareStatement("select price,bookname from book order by price asc");
			ResultSet result=stat.executeQuery();
			while(result.next())
			{
				Book b1=new Book();
				b1.setPrice(result.getInt(1));
				b1.setBookname(result.getString(2));
				l2hlist.add(b1);
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
		return l2hlist;
	}
	@Override
	public List<Book> highToLow() 
	{
		List<Book> h2llist=new ArrayList<Book>();
		try
		{
			stat=con1.prepareStatement("select price,bookname from book order by price desc");
			ResultSet result=stat.executeQuery();
			while(result.next())
			{
				Book b1=new Book();
				b1.setPrice(result.getInt(1));
				b1.setBookname(result.getString(2));
				h2llist.add(b1);
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
		return h2llist;
	}
	@Override
	public List<Book> bestSelling() 
	{
		List<Book> bestlist=new ArrayList<Book>();
		try
		{
			stat=con1.prepareStatement("select copiessold,bookname from book order by copiessold desc");
			ResultSet result=stat.executeQuery();
			while(result.next())
			{
				Book b1=new Book();
				b1.setNoOfCopies(result.getInt(1));
				b1.setBookname(result.getString(2));
				bestlist.add(b1);
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
		return bestlist;
	}
	
	@Override
	public HashMap<Integer,Book> newBooks() 
	{
		HashMap<Integer,Book> newBooks=new HashMap<Integer,Book>();
		try
		{
			stat=con1.prepareStatement("(select bookid,bookname from book order by bookid desc limit 3) order by bookid asc");
			ResultSet result=stat.executeQuery();
			while(result.next())
			{
				Book b1=new Book();
				b1.setBookid(result.getInt(1));
				b1.setBookname(result.getString(2));
				newBooks.put(b1.getBookid(),b1);
			}
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		return newBooks;
		
		
	}
	
	@Override
	public void selectBook(Book sbook) 
	{
		try 
		{
			stat=con1.prepareStatement("select * from book where bookid=?");
			stat.setInt(1, sbook.getBookid());
			ResultSet result=stat.executeQuery();
			if(result!=null)
			{
				System.out.println("Book selected successfully");
			}
			else
			{
				System.out.println("Book not found");
			}
			
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
	}
	
	@Override
	public HashMap<Integer, Book> getBook() 
	{
		HashMap<Integer, Book> booklist=new HashMap<Integer,Book>();
		try {
		stat=con1.prepareStatement("select * from book");
		ResultSet result=stat.executeQuery();
			while(result.next())
			{
				Book b1=new Book();
				b1.setBookid(result.getInt(1));
				b1.setBookname(result.getString(2));
				b1.setDescription(result.getString(3));
				b1.setAuthorName(result.getString(4));
				b1.setPrice(result.getInt(5));
				b1.setNoOfCopies(result.getInt(6));
				b1.setGenre(result.getString(7));
				booklist.put(b1.getBookid(),b1);
				
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return booklist;
		
	}	

	
}
