package com.gl.main.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import com.gl.main.connect.DataConnect;
import com.gl.main.pojo.Admin;

public class AdminDAOImpl implements AdminDAO
{
	static Scanner sc;
	private Connection con1;
	private PreparedStatement stat;
	public AdminDAOImpl()
	{
		con1=DataConnect.getConnect();
	}
	
	@Override
	public void register(Admin admin) 
	{
		try
		{
			stat=con1.prepareStatement("insert into admin values(?,?,?)");
			stat.setInt(1,admin.getAdminId());
			stat.setString(2, admin.getAdminName());
			stat.setString(3, admin.getPassword());
			int result=stat.executeUpdate();
			if(result>0)
			{
				System.out.println("Data inserted");
			}
		}
		catch(SQLException ex)
		{
			System.out.println(ex.getMessage());
		}
	}

	@Override
	public List<Admin> login()
	{
		List<Admin> adminlist=new ArrayList<Admin>();
		try
		{
			stat=con1.prepareStatement("select adminid,password from admin");
			ResultSet result=stat.executeQuery();
			while(result.next())
			{
				Admin a1=new Admin();
				a1.setAdminId(result.getInt(1));
				a1.setPassword(result.getString(2));
				adminlist.add(a1);
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		return adminlist;
	}
}
