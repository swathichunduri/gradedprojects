package com.gl.main.dao;
import java.util.*;
import com.gl.main.pojo.User;

public interface UserDAO 
{
	public void register(User user);
	public List<User> login();
}
