package com.gl.main.exceptions;

public class NegativeNumberException extends Exception
{
	public String getMessage()
	{
		return "Trying to enter negative number.\nPlease enter a positive number.";
	}
}
