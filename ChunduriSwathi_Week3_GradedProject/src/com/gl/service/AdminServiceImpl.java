package com.gl.service;
import java.util.*;
import com.gl.main.dao.AdminDAOImpl;
import com.gl.main.pojo.Admin;

public class AdminServiceImpl implements AdminService
{
	static Scanner sc;
	AdminDAOImpl adao=new AdminDAOImpl();
	public AdminServiceImpl()
	{
		sc=new Scanner(System.in);
	}
	
	@Override
	public void adminRegister() 
	{
		Admin a1=new Admin();
		System.out.println("Enter adminid ");
		a1.setAdminId(sc.nextInt());
		System.out.println("Enter admin name ");
		a1.setAdminName(sc.next());
		System.out.println("Enter password ");
		a1.setPassword(sc.next());
		adao.register(a1);
	}

	@Override
	public boolean adminLogin(boolean value)
	{
		List<Admin> adminlist=adao.login();
		System.out.println("Enter valid adminid ");
		int adminid=sc.nextInt();
		System.out.println("Enter valid password ");
		String pwd=sc.next();
		for(Admin a:adminlist)
			if((a.getAdminId()==adminid) && a.getPassword().equals(pwd))
			{
				System.out.println("Login Successfully");
				return true;
			}
			else
			{
				value=false;
			}
		return false;
	}

}
