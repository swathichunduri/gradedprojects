package com.gl.service;

public interface BookService 
{
	public void addBook();
	public void deleteBook();
	public void modifyBook();
	public void displayAll();
	public void totalCountOfBooks();
	public void seeGenreBooks();
	public void arrangeBooks();
	public void newBooks();
	public void getBook();
	public void selectBook();
}
