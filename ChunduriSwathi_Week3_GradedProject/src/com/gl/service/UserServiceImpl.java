package com.gl.service;
import java.util.*;

import com.gl.main.dao.UserDAOImpl;
import com.gl.main.pojo.User;

public class UserServiceImpl implements UserService
{
	static Scanner sc;
	UserDAOImpl udao;
	public UserServiceImpl()
	{
		sc=new Scanner(System.in);
		udao=new UserDAOImpl();
	}
	@Override
	public void userRegister() {
		User u1=new User();
		System.out.println("Enter userid ");
		u1.setUserId(sc.nextInt());
		System.out.println("Enter user name ");
		u1.setUsername(sc.next());
		System.out.println("Enter password ");
		u1.setPassword(sc.next());
		udao.register(u1);
	}

	@Override
	public boolean UserLogin(boolean value) 
	{
		List<User> userlist=udao.login();
		System.out.println("Enter valid userid ");
		int userid=sc.nextInt();
		System.out.println("Enter valid password ");
		String pwd=sc.next();
		for(User u:userlist)
			if((u.getUserId()==userid) && u.getPassword().equals(pwd))
			{
				System.out.println("Login Successfully");
				return true;
			}
			else
			{
				value=false;
			}
		return false;
	}
}
