package com.gl.service;
import java.util.*;

import com.gl.main.dao.BookDAOImpl;
import com.gl.main.pojo.Book;
public class BookServiceImpl implements BookService
{
	static Scanner sc;
	Book book;
	BookDAOImpl bdao;
	public BookServiceImpl()
	{
		sc=new Scanner(System.in);
		book=new Book();
		bdao=new BookDAOImpl();
	}
	@Override
	public void addBook() 
	{
		System.out.println("Enter no of books u want to add ");
		int noofbooks=sc.nextInt();
		for(int x=1;x<=noofbooks;x++)
		{
			Book b1=new Book();
			System.out.println("Enter Book code");
			b1.setBookid(sc.nextInt());
			System.out.println("Enter Book Name ");
			b1.setBookname(sc.next());
			System.out.println("Enter Book description ");
			b1.setDescription(sc.next());
			System.out.println("Enter Author name ");
			b1.setAuthorName(sc.next());
			System.out.println("Enter price ");
			b1.setPrice(sc.nextInt());
			System.out.println("Enter number of books sold ");
			b1.setNoOfCopies(sc.nextInt());
			System.out.println("Enter whether its Autobiography are not ");
			b1.setGenre(sc.next());
			bdao.addBookDetails(b1);
		}
	}

	@Override
	public void deleteBook() 
	{	
		System.out.println("Enter Book code which u want to delete");
		int bookid=sc.nextInt();
		Book deletebook=new Book();
		deletebook.setBookid(bookid);
		bdao.deleteBookDetails(deletebook);
	}

	@Override
	public void modifyBook()
	{
		System.out.println("Enter the bookid u want to modify");
		int bookid=sc.nextInt();
		System.out.println("Enter new price of the book");
		int newprice=sc.nextInt();
		Book mbook=new Book();
		mbook.setBookid(bookid);
		mbook.setPrice(newprice);
		bdao.modifyPrice(mbook);
	}

	@Override
	public void displayAll() 
	{
		HashMap<Integer,Book> book=bdao.displayAllBooks();
		for(Integer a1:book.keySet())
		{
			Book b1=book.get(a1);
			System.out.println("ID"+b1.getBookid()+"--->Rs."+b1.getBookname());
		}
	}

	@Override
	public void totalCountOfBooks() 
	{
		List<Book> booklist=bdao.totalCountOfBooks();
		for(Book b1:booklist)
		{
			System.out.println("Total Books:"+(100-b1.getNoOfCopies())+"---->"+b1.getBookname());
		}
		
	}

	@Override
	public void seeGenreBooks() 
	{
		List<Book> genrebook=bdao.seeGenreBooks();
		for(Book b1:genrebook)
		{
			System.out.println(b1.getBookname());
		}
	}

	@Override
	public void arrangeBooks() 
	{
		System.out.println("1.Arrange by price(low-high)");
		System.out.println("2.Arrange by price(high-low)");
		System.out.println("3.Arrange by best selling");
		System.out.println("Enter your choice");
		int choice=sc.nextInt();
		if(choice==1)
		{
			lowToHigh();
		}
		else if(choice==2)
		{
			highToLow();
		}
		else if(choice==3)
		{
			bestSelling();
		}
		else
		{
			System.out.println("No more possibilities to arrange");
		}
	}
	private void bestSelling() 
	{
		List<Book> bestlist=bdao.bestSelling();
		for(Book b1:bestlist)
		{
			System.out.println("Copies Sold:"+b1.getNoOfCopies()+"--->"+b1.getBookname());
		}
		
	}
	private void highToLow()
	{
		List<Book> h2llist=bdao.highToLow();
		for(Book b1:h2llist)
		{
			System.out.println("Rs."+b1.getPrice()+"--->"+b1.getBookname());
		}
	}
	private void lowToHigh() 
	{
		List<Book> l2hlist=bdao.lowToHigh();
		for(Book b1:l2hlist)
		{
			System.out.println("Rs."+b1.getPrice()+"--->"+b1.getBookname());
		}
	}
	@Override
	public void newBooks() 
	{
		HashMap<Integer,Book> newlist=bdao.newBooks();
		for(Integer a1: newlist.keySet())
		{
			Book b1=newlist.get(a1);
			System.out.println("ID:"+b1.getBookid()+"--->"+b1.getBookname());
		}
	}
	@Override
	public void getBook() 
	{
		HashMap<Integer,Book> book=bdao.getBook();
		for(Integer a1:book.keySet())
		{
			Book b1=book.get(a1);
			System.out.println("ID:"+b1.getBookid()+"--->"+b1.getBookname());
		}
	}
	@Override
	public void selectBook()
	{	
		System.out.println("Enter the book id u want to select");
		int bookid=sc.nextInt();
		Book selectbook=new Book();
		selectbook.setBookid(bookid);
		bdao.selectBook(selectbook);
	}
	
}
