//creation of database
create database Magic_Of_Books;

//using database
use maic_of-books;

//creating book table
create table book
(
	bookid int primary key,
	bookname varchar(30),
	description varchar(50),
	authorname varchar(30),
	price int,
	copiessold int,
	Autobiography varchar(30)
);

//creating user table
create table user
(
	userid int primary key,
	username varchar(30),
	password varchar(30)
);

//create admin table
create table admin
(
	adminid int primary key,
	adminname varchar(30),
	password varchar(30)
);
 
//description of books
desc book;

//description of user
desc user;

//description of admin
desc admin;

//inserting values in user
insert into user values(1,"user1","user1");

//inserting values in admin
insert into admin values(1,"admin1","admin1");

//insering values into book
insert into book values(4," Little Women"," Women oriented story","Louis May Alcott",234,18,"no");

//deleting a book
delete from book where bookid=?;

//updating a book
update book set price=? where bookid=?;

//dispalying book
select * from book;

//displaying autobiogarphies
select autobiography from book where autobiography like 'y%';

//arranging books from low-high price
select price,bookname from book order by price asc;

//arranging books from high-low price
select price,bookname from book order by price desc;

//arranging books according the highest sales
select copiesSold,bookname from book order by copiessold desc;

//lastest books
(select bookid,bookname from book order by bookid desc limit 3) order by bookid asc;

//selecting a specific book
select bookname from book where bookid=?;