package Main;
import java.io.IOException;
import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import dao.MagicOfBooks;
import dao.UserDAOImpl;
import exception.NegativeNumberException;

//Extending the Main class from Thread class
public class Main extends Thread
{
	private Scanner sc;
	UserDAOImpl udao;
	MagicOfBooks mobj;
	
	public Main()
	{
		sc=new Scanner(System.in);
		udao=new UserDAOImpl();
		mobj=new MagicOfBooks(); 
	}
	
	public void run()
	{
		int choice=0;
		do
		{
			System.out.println("----------MAGIC OF BOOKS----------");
			System.out.println("1.Register");
			System.out.println("2.Login");
			System.out.println("0.Exit");
			System.out.println("Enter your choice");
			choice=sc.nextInt();
			switch(choice)
			{
				case 1:
						udao.register();
				case 2:
						udao.login();
						break;
			}
		}while(choice!=0);
	}
	
	public void menu() throws NegativeNumberException
	{
		int choice=0;
		do
		{
			System.out.println("1.New Books");
			System.out.println("2.Favourite Books");
			System.out.println("3.Completed Books");
			System.out.println("4.Select Book");
			System.out.println("5.Get Book");
			System.out.println("0.Exit");
			System.out.println("Enter your choice:");
			choice=sc.nextInt();
			if(choice<0)
			{
				throw new NegativeNumberException();
			}
			switch(choice)
			{
				case 1:
						mobj.newBooks();
						break;
				case 2:
						mobj.favourite();
						break;
				case 3:
						mobj.completed();
						break;
				case 4:
						try 
						{
							mobj.selectbook();
						} 
						catch (NegativeNumberException e) {
							System.out.println(e.getMessage());
						}
						
						break;
				case 5:
						try 
						{
							mobj.getbook();
						} 
						catch (NegativeNumberException e) {
							System.out.println(e.getMessage());
						}
						break;
				case 0:
						System.out.println("Thanks for using this application.");
						System.out.println("Press "+0+" again to exit.");
			}
		}while(choice!=0);
	}
	

	public static void main(String[] args) 
	{
		boolean append = true;
        FileHandler handler = null;
		try 
		{
			handler = new FileHandler("C:\\Users\\swathi.chunduri\\eclipse-workspace\\ChunduriSwathi_Week2_GradedProject\\src\\log.txt", append);
		} 
		catch (SecurityException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
        logger.addHandler(handler);
        logger.severe("severe message");
        logger.warning("warning message");
        logger.info("info message");
        logger.config("config message");
        logger.fine("fine message");
        logger.finer("finer message");
        logger.finest("finest message");
        //Initializing the Main method 
		Main mainobj=new Main();
		mainobj.start();
	}
}

