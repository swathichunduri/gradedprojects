package dao;
import java.util.*;

import exception.NegativeNumberException;
import pojo.Book;
public class MagicOfBooks implements BookDAO
{
	static Scanner sc;
	Set<Book> booklist;
	
	public MagicOfBooks()
	{
		sc=new Scanner(System.in);
		booklist =new HashSet<Book>();
		//Initialization of booklist set 
		booklist.add(new Book("Goodnight Moon","Margaret","A good night story",111));
		booklist.add(new Book("The Very Hungry caterpillar","Eric Caele","tells evolution of caterpillar",112));
		booklist.add(new Book("where the Wild Things Are","Maurice Sendak","A boy's story ",113));
		booklist.add(new Book("The Cat in the Hat","Seuss","A anthropomorphic cat",114));
		booklist.add(new Book("Charlotte's Web","E.B.White","A true freinds story",115));
		booklist.add(new Book("Harold and the Purple Crayon","Crockett Johnson","A magical Crayon",116));
		booklist.add(new Book("Charlie and the Chocolate Factory","Roald Dahl","A boys love towards chocolates",117));
		booklist.add(new Book("Little Women","Louis May Alcott","Women oriented story",118));
		booklist.add(new Book("Harry Potter and the Philosopher's Stone","K.Rowling","Harry Potter series-1",119));
		booklist.add(new Book("The Chronices of Narina","C.S.Lewis","The Lion,the Witch and the Wardrobe",120));
	}
	
	public void newBooks()
	{
		Set<Book> newBooks=new HashSet<Book>();
		System.out.println("New Books are");
		newBooks.add(new Book("Goodnight Moon","Margaret","A good night story",111));
		newBooks.add(new Book("Charlie and the Chocolate Factory","Roald Dahl","A boys love towards chocolates",117));
		newBooks.forEach(
				t->
				{
					System.out.println(t.getBookName());
				});	
	}
	
	public void favourite()
	{
		Set<Book> favourite=new HashSet<Book>();
		System.out.println("Your favourite books are");
		favourite.add(new Book("The Very Hungry caterpillar","Eric Caele","tells evolution of caterpillar",112));
		favourite.add(new Book("Little Women","Louis May Alcott","Women oriented story",118));
		favourite.forEach(
				t->
				{
					System.out.println(t.getBookName());
				});	
	}
	
	public void completed()
	{
		System.out.println("Completed books are");
		Set<Book> completed=new HashSet<Book>();
		completed.add(new Book("The Cat in the Hat","Seuss","A anthropomorphic cat",114));
		completed.forEach(
				t->
				{
					System.out.println(t.getBookName());
				});	
	}
	
	public void selectbook() throws NegativeNumberException
	{
		System.out.println("Enter the bookId: ");
		int book_id=sc.nextInt();
		//checks and throws the exception
		if(book_id<0) 
		{
			throw new NegativeNumberException();
		}
		int flag=0;
		for(Book bookobj:booklist)		
		{
			if(bookobj.getBookId()==book_id)
			{
				flag++;
				System.out.println("Book selected sucessfully");	
			}
		}
		if(flag==0)
		{
			System.out.println("Sorry,Book not found\nBookid starts from 111");
		}
		
	}
	
	public void getbook() throws NegativeNumberException
	{
		int flag=0;
		System.out.println("Enter the bookId: ");
		int book_id=sc.nextInt();
		//checks and throws the exception
		if(book_id<0)
		{
			throw new NegativeNumberException();
		}
		for(Book bookobj:booklist)
		{
			if(bookobj.getBookId()==book_id)
			{
				flag++;
				System.out.println("Book Author is "+bookobj.getAuthorName());
				System.out.println("Book Description is "+bookobj.getDescription());
			}
		}
		if(flag==0)
		{
			System.out.println("Sorry,Book not found\nBookid starts from 111");
		}
	}
}
