package dao;
import java.util.*;
import Main.Main;
import exception.NegativeNumberException;
import pojo.User;
public class UserDAOImpl implements UserDAO
{
	static Scanner sc;
	//declaration of user set
	Set <User> user;
	public UserDAOImpl()
	{
		sc=new Scanner(System.in);
		user=new HashSet<User>();
	}
	
	@Override
	public void register() 
	{
		System.out.println("----------REGISTRATION----------");
		User userobj=new User();
		System.out.println("Enter your username: ");
		userobj.setUserName(sc.next());
		System.out.println("Enter your password: ");
		userobj.setPassword(sc.next());
		System.out.println("Enter your EmailId: ");
		userobj.setEmailid(sc.next());
		user.add(userobj);
		//adding users to the set 
		System.out.println("Registred Successfully.....");
		System.out.println("Please login to the application after registration.");
	}
	
	//if the user gives correct username and password then only the application will be opened
	@Override
	public void login()
	{
		System.out.println("----------LOGIN----------");
		System.out.println("Enter valid username: ");
		String username=sc.next();
		System.out.println("Enter valid password: ");
		String password=sc.next();
		int flag=0;
		for(User uobj:user)
		{
			if(uobj.getUserName().equals(username) && uobj.getPassword().equals(password))
			{
					flag++;
					System.out.println("Logined Successfully.....");
					//if the user logins successfully the main menu will be opened.
					Main mainobj=new Main();
				try {
					mainobj.menu();
				}
				catch(NegativeNumberException e)
				{
					System.out.println(e.getMessage());
					System.out.println("Please login again and  continue");
				}
			}
		}
		if(flag==0)
		{
			System.out.println("Please Enter valid username and password to login");
		}
		
	}

}

