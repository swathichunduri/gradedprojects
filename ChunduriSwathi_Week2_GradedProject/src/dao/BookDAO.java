package dao;

import exception.NegativeNumberException;

public interface BookDAO 
{
	public void newBooks();
	public void favourite();
	public void completed();
	//these methods throws the exception when negative number is given
	public void selectbook() throws NegativeNumberException;
	public void getbook() throws NegativeNumberException;
}
